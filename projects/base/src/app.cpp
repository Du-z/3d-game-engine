#include "app.h"

#include "app\logger.h"
#include "app\profiler\profile.h"
#include "app\utilities.h"

#include "app\resMgr.h"
#include "app\eventMgr.h"
#include "app\inputMgr.h"
#include "app\gameWorld.h"

App* App::mInstance = NULL;

App* App::GetInstance()
{
	if (mInstance == NULL) 
		mInstance = new App();

	return mInstance;
}

App::App()
{
}

App::~App()
{
	mInstance = NULL;
}

Error::Code App::setup()
{
	logInfo("Starting App Setup");

	Error::Code errorCode = Error::NONE;

	errorCode = rendererSetup();
	if(errorCode != Error::NONE)
		return errorCode;

	dynamicWorldSetup();

	InputMgr::GetInstance()->setup(mWindow);

	GameWorld::GetInstance()->setup("demo/gameWorld.json");

	logInfo("Finished App Setup");
	return errorCode;
}

void App::dynamicWorldSetup()
{
	// Build the broadphase
	broadphase = new btDbvtBroadphase();

	// Set up the collision configuration and dispatcher
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	// The actual physics solver
	solver = new btSequentialImpulseConstraintSolver;

	// The world.
	mDynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,broadphase,solver,collisionConfiguration);
	mDynamicsWorld->setGravity(btVector3(0,-10,0));
}

Error::Code App::rendererSetup()
{
	// Initialise GLFW
	if( !glfwInit() )
	{
		logError("Failed to initialize GLFW");
		return Error::GLFW_INIT_FAIL;
	}

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

	// Open a window and create its OpenGL context
	mWindow = glfwCreateWindow( 1280, 720, "Technical Demo", NULL, NULL);
	if(mWindow == NULL)
	{
		logError("Failed to open GLFW window");
		glfwTerminate();
		return Error::GLFW_CREATE_WINDOW_FAIL;
	}

	glfwMakeContextCurrent(mWindow);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed in core profile
	if (glewInit() != GLEW_OK) {
		logError("Failed to initialize GLEW");
		return Error::GLEW_INIT_FAIL;
	}

	glfwSetInputMode(mWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(mWindow, GLFW_STICKY_MOUSE_BUTTONS, GL_TRUE);
	glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LEQUAL);
	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	// Enable blending
	glEnable(GL_BLEND);

	// set the start time
	glfwSetTime(0);

	// turn off vsync
	glfwSwapInterval(0);

	return Error::NONE;
}

void App::loop()
{		
	double lastTime = glfwGetTime();
	
	while (
		mIsRunning &&
		glfwGetKey(mWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		!glfwWindowShouldClose(mWindow)
		)
	{
		profile();

		double currentTime = glfwGetTime();
		float deltaT = currentTime - lastTime;
		lastTime = currentTime;

		glfwPollEvents();
		EventMgr::GetInstance()->notifySubscribers();

		// SIMULATE
		GameWorld::GetInstance()->tick(deltaT);
	
		dynamicWorldStep(deltaT);
		
		// Do OpenGl error checking
		GLenum glError;
		while((glError = glGetError()) != GL_NO_ERROR)
		{
			//logError("OpenGL error: " << glError);
		}

		// tell the profiler that the frame has ended
		profileEndFrame();
	}
}

void App::dynamicWorldStep(const float deltaT)
{
	profile();

	const float fixedTimeStep = 1/120.f;
	const int maxSubSteps  = 10;
	mDynamicsWorld->stepSimulation(deltaT, maxSubSteps, fixedTimeStep);	
}

void App::dispose()
{
	ResMgr::GetInstance()->disposeAll();

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}