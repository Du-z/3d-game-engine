#ifndef ERROR_CODES_H
#define ERROR_CODES_H

namespace Error
{
	enum Code
	{
		UNSET = -1,

		NONE,

		MUTEX_ALREADY_EXISTS,

		GLFW_INIT_FAIL,
		GLFW_CREATE_WINDOW_FAIL,
		GLEW_INIT_FAIL,

		COUNT
	};
}

#endif // ERROR_CODES_H