#include "app.h"

#include <windows.h>

#include "errorCodes.h"

int main(int argc, char *argv[])
{
	// make sure that this is the only instance of the program
	HANDLE mutex = CreateMutex(NULL, FALSE, TEXT("Global\\{694FF6FD-CCAB-4AB9-AC5D-86987321EEE5}"));
	if(GetLastError() == ERROR_ALREADY_EXISTS)
		return Error::MUTEX_ALREADY_EXISTS;

	
	App* app = App::GetInstance();

	Error::Code errorCode = Error::NONE;
	errorCode = app->setup();
	if(errorCode != Error::NONE)
		return errorCode;


	app->loop();
	app->dispose();

	delete app;
	app = 0;

	CloseHandle(mutex);
	return EXIT_SUCCESS;
}