#include "lightMgr.h"

#include "resMgr.h"
#include "logger.h"
#include "utilities.h"

LightMgr* LightMgr::mInstance = NULL;

LightMgr* LightMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new LightMgr();

	return mInstance;
}

LightMgr::LightMgr()
{
}

LightMgr::~LightMgr()
{
	mInstance = NULL;
}

void LightMgr::setup(const std::string& dir)
{

	Json::Value val = *ResMgr::GetInstance()->getJson(dir);

	Json::Value lightInfoList = val["lights"];

	size_t size = lightInfoList.size();

	for(size_t i = 0; i < size; ++i)
	{
		LightData light;

		Json::Value lightInfo = lightInfoList[i];

		light.pos = glm::vec3(lightInfo["pos"]["x"].asDouble(), lightInfo["pos"]["y"].asDouble(), lightInfo["pos"]["z"].asDouble());
		light.type = strToLightType(lightInfo["type"].asString());
		light.power = lightInfo["power"].asDouble();

		mLightList.push_back(light);
	}

	// we don't need the json file any more, we have loaded all we need from it
	ResMgr::GetInstance()->disposeJson(dir);
	
}

Light::Type LightMgr::strToLightType(const std::string& str)
{
	if(strcmp(str.c_str(), "point") == 0)
		return Light::POINT;
	if(strcmp(str.c_str(), "spot") == 0)
		return Light::SPOT;
	if(strcmp(str.c_str(), "dir") == 0)
		return Light::DIRECTIONAL;

	logWarn("Invalid Light Type: " << str);
	return Light::POINT;
}

void LightMgr::getClosestLights(Light::Type typeToGet, const int count, std::vector<LightData*>& lightList)
{	
	lightList.reserve(count);

	for(int i = 0; i < mLightList.size(); i++)
	{
		if(mLightList[i].type == typeToGet)
			lightList.push_back(&mLightList[i]);
	}
}