#include "gameWorld.h"

#include "gameWorld/gameObj/gameComponentMgr.h"

#include "app.h"

GameWorld* GameWorld::mInstance = NULL;

GameWorld* GameWorld::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new GameWorld();

	return mInstance;
}

GameWorld::GameWorld()
{

}

GameWorld::~GameWorld()
{
	mInstance = NULL;
}

void GameWorld::setup(const std::string& dir)
{	
	Json::Value worldList = (*ResMgr::GetInstance()->getJson(dir))["gameWorld"];

	for(size_t i = 0; i < worldList.size(); ++i)
	{
		Json::Value objectList = (*ResMgr::GetInstance()->getJson(worldList[i]["dir"].asString()))["object"]; 

		for(size_t j = 0; j < objectList.size(); ++j)
		{
			GameObj* gameObj = new GameObj();

			glm::vec3 pos = glm::vec3(worldList[i]["pos"]["x"].asDouble(), worldList[i]["pos"]["y"].asDouble(), worldList[i]["pos"]["z"].asDouble());

			std::string nameId = worldList[i]["name"].asString();
			std::vector<std::string> nameIdList;
			nameIdList.push_back(nameId);
			
			gameObj->setup(nameIdList, objectList[j], pos, NULL);

			addObj(gameObj); 
		}
	}

	ResMgr::GetInstance()->disposeAllJsons();

	EventMgr::GetInstance()->addSubscriber(Event::KEY_RELEASED, this);
}

GameObj* GameWorld::addObj(GameObj* gameObj)
{
	mGameObjList.push_back(gameObj);

	return gameObj;
}

void GameWorld::eventReceiver(ABEvent* theEvent)
{
	// Check the event type
	if(theEvent->getEventType() == Event::KEY_RELEASED)
	{
		KeyReleased* key = static_cast<KeyReleased*>(theEvent);
		switch (key->getKey())
		{
		case GLFW_KEY_F1:
			DrawableCompMgr::GetInstance()->setDrawWireFrame(!DrawableCompMgr::GetInstance()->getDrawWireFrame());
			break;
		case GLFW_KEY_F2:
			DrawableCompMgr::GetInstance()->setDrawBulletDebug(!DrawableCompMgr::GetInstance()->getDrawBulletDebug());
			break;
		default:
			return;
		}
	}
}

void GameWorld::tick(const float deltaT)
{
	profile();

	GameCompMgr::GetInstance()->tick(deltaT);
}