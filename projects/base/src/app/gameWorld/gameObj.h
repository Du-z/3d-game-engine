#ifndef GAME_OBJ_H
#define GAME_OBJ_H

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/transform.hpp>

#include "app\resMgr.h"
#include "app\utilities.h"
#include "app\logger.h"
#include "app\profiler\profile.h"

class ABGameObjComp;

class GameObj
{
public:
	GameObj();
	GameObj(const std::vector<std::string>& parentsNameIdList, const Json::Value& object, const glm::vec3& worldPos, GameObj* parentGameObj);
	~GameObj();

	void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& object, const glm::vec3& worldPos, GameObj* parentGameObj);

	bool compareId(unsigned int idToTest); // returns true if the IDs are the same
	static unsigned int calculateId(const std::vector<std::string>& strList);
	static unsigned int calculateId(const std::string& str);

	static std::string strListToStr(const std::vector<std::string>& strList);

	glm::vec3 getLocalTranslation(){return glm::vec3(mMatrix[3]);}
	glm::vec3 getDerivedTranslation(){return glm::vec3(mDerivedMatrix[3]);}

private:
	PROP_G(unsigned int, mId, Id);
	void setId(const std::vector<std::string>& strList);
	void setId(const std::string& str);

	PROP_GR(glm::mat4, mMatrix, Matrix);
	PROP_GR(glm::mat4, mDerivedMatrix, DerivedMatrix);

	PROP_G(GameObj*, mParentGameObj, ParentGameObj);
};

#endif // GAME_OBJ_H