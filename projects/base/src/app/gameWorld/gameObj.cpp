#include "gameObj.h"

#include "app/gameWorld.h"

#include "gameObj/gameComponentMgr.h"

GameObj::GameObj()
{
	mMatrix = glm::mat4(1);
}

GameObj::GameObj(const std::vector<std::string>& parentsNameIdList, const Json::Value& object, const glm::vec3& worldPos, GameObj* parentGameObj)
{
	mMatrix = glm::mat4(1);
	mDerivedMatrix = glm::mat4(1);
	setup(parentsNameIdList, object, worldPos, parentGameObj);
}

GameObj::~GameObj()
{

}

void GameObj::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& object, const glm::vec3& worldPos, GameObj* parentGameObj)
{
	std::string nameId = object["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);
	
	mParentGameObj = parentGameObj;

	if(mParentGameObj == NULL)
	{
		glm::vec3 pos = worldPos + glm::vec3(object["pos"]["x"].asDouble(), object["pos"]["y"].asDouble(), object["pos"]["z"].asDouble());

		// set translation
		mMatrix[3] = glm::vec4(pos, 1);
		mDerivedMatrix = mMatrix;
	}		
	else if(mParentGameObj != NULL)
	{
		// set translation
		mMatrix[3] = glm::vec4(object["pos"]["x"].asDouble(), object["pos"]["y"].asDouble(), object["pos"]["z"].asDouble(), 1);
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;
	}

	// setup components
	GameCompMgr* componentMgr = GameCompMgr::GetInstance();
	Json::Value componentList = object["components"];
	for(size_t i = 0; i < componentList.size(); ++i)
	{ 		
		componentMgr->add(nameIdList, componentList[i], this);
	}

	// setup child objects
	Json::Value objectList = object["object"];
	GameWorld* gameWorld = GameWorld::GetInstance();
	for(size_t j = 0; j < objectList.size(); ++j)
	{
		GameObj* gameObj = new GameObj();

		gameObj->setup(nameIdList, objectList[j], glm::vec3(0), this);

		gameWorld->addObj(gameObj); 
	}
}

void GameObj::setId(const std::vector<std::string>& strList)
{
	mId = calculateId(strList);
}

void GameObj::setId(const std::string& str)
{
	mId = calculateId(str);
}

unsigned int GameObj::calculateId(const std::vector<std::string>& strList)
{
	std::string str = strListToStr(strList);
	
	return calculateId(str);
}

unsigned int GameObj::calculateId(const std::string& str)
{
	return Utilities::hash(str);
}

std::string GameObj::strListToStr(const std::vector<std::string>& strList)
{
	std::string str;

	for(int i = 0; i < strList.size(); i++)
	{
		str += strList[i];
	}

	return str;
}

bool GameObj::compareId(unsigned int idToTest)
{
	return mId == idToTest;
}