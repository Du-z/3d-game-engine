#ifndef GAME_COMPONENT_H
#define GAME_COMPONENT_H

#include "app\gameWorld\gameObj.h"

class ABGameObjComp : public ABEventReciver
{
public:
	ABGameObjComp();
	virtual ~ABGameObjComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL) = 0;

	virtual void tick(const float deltaT) = 0;

	virtual void eventReceiver(ABEvent* theEvent) = 0;

	enum Type
	{
		UNSET = -1,

		LUA_SCRIPT,
		PHYSICS_CONSTRAINT,
		PHYSICS_BODY,
		UI_TEXT,
		UI_PROFILER,
		PARTICLE_EMITTER,
		CAMERA,
		DRAWABLE,

		COUNT
	};

	bool compareId(unsigned int idToTest); // returns true if the IDs are the same
	static unsigned int calculateId(const std::vector<std::string>& strList);
	static unsigned int calculateId(const std::string& str);

	static std::string strListToStr(const std::vector<std::string>& strList);

	glm::vec3 getLocalTranslation(){return glm::vec3(mMatrix[3]);}
	glm::vec3 getDerivedTranslation(){return glm::vec3(mDerivedMatrix[3]);}

protected:
	PROP_G(Type, mType, Type);

	PROP_G(unsigned int, mId, Id);
	void setId(const std::vector<std::string>& strList);
	void setId(const std::string& str);

	PROP_GR(glm::mat4, mMatrix, Matrix);
	PROP_GR(glm::mat4, mDerivedMatrix, DerivedMatrix);

	PROP_G(GameObj*, mParentGameObj, ParentGameObj);
	PROP_G(ABGameObjComp*, mParentGameComp, ParentGameComp);
};

#endif // GAME_COMPONENT_H