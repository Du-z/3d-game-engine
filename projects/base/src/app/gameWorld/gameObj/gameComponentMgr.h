#ifndef GAME_COMPONENT_MGR_H
#define GAME_COMPONENT_MGR_H

#include "abGameComponent.h"

#include "gameComponent/drawableCompMgr.h"
#include "gameComponent/luaScriptCompMgr.h"
#include "gameComponent/physicsBodyCompMgr.h"
#include "gameComponent/uiProfilerCompMgr.h"
#include "gameComponent/uiTextCompMgr.h"
#include "gameComponent/physicsConstraintCompMgr.h"
#include "gameComponent/cameraCompMgr.h"
#include "gameComponent/particleEmitterCompMgr.h"

class GameCompMgr
{
public:
	~GameCompMgr();
	static GameCompMgr* GetInstance();

	void add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	void tick(const float deltaT);

private:
	GameCompMgr();
	static GameCompMgr* mInstance;

	ABGameObjComp::Type strToGameComponentEnum(const std::string& str);
};

#endif // GAME_COMPONENT_MGR_H