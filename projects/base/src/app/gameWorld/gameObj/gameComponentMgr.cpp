#include "gameComponentMgr.h"

GameCompMgr* GameCompMgr::mInstance = NULL;

GameCompMgr* GameCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new GameCompMgr();

	return mInstance;
}

GameCompMgr::GameCompMgr()
{
}

GameCompMgr::~GameCompMgr()
{
	mInstance = NULL;
}

void GameCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	ABGameObjComp::Type type = strToGameComponentEnum(component["type"].asString());

	ABGameObjComp* gameObjComp = NULL;

	switch (type)
	{
	case ABGameObjComp::LUA_SCRIPT:
		gameObjComp = LuaScriptCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::PHYSICS_CONSTRAINT:
		gameObjComp = PhysicsConstraintCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::PHYSICS_BODY:
		gameObjComp = PhysicsBodyCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::UI_TEXT:
		gameObjComp = UiTextCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::UI_PROFILER:
		gameObjComp = UiProfilerCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::CAMERA:
		gameObjComp = CameraCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::PARTICLE_EMITTER:
		gameObjComp = ParticleEmitterCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	case ABGameObjComp::DRAWABLE:
		gameObjComp = DrawableCompMgr::GetInstance()->add(parentsNameIdList, component, parentGameObj, parentGameComp);
		break;
	default:
		logWarn("'" << component["type"].asString() << "' is not a valid component type for object '" << GameObj::strListToStr(parentsNameIdList) << "'.")
		break;
	}


	// setup child components
	GameCompMgr* componentMgr = GameCompMgr::GetInstance();

	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);

	Json::Value componentList = component["components"];
	for(size_t i = 0; i < componentList.size(); ++i)
	{ 		
		componentMgr->add(nameIdList, componentList[i], parentGameObj, gameObjComp);
	}
}

void GameCompMgr::tick(const float deltaT)
{
	profile();

	LuaScriptCompMgr::GetInstance()->tick(deltaT);
	PhysicsBodyCompMgr::GetInstance()->tick(deltaT);
	PhysicsConstraintCompMgr::GetInstance()->tick(deltaT);
	UiProfilerCompMgr::GetInstance()->tick(deltaT);
	ParticleEmitterCompMgr::GetInstance()->tick(deltaT);
	CameraCompMgr::GetInstance()->tick(deltaT);
	DrawableCompMgr::GetInstance()->tick(deltaT);
}

ABGameObjComp::Type GameCompMgr::strToGameComponentEnum(const std::string& str)
{
	if(strcmp(str.c_str(), "luaScript") == 0)
		return ABGameObjComp::Type::LUA_SCRIPT;
	if(strcmp(str.c_str(), "physicsConstraint") == 0)
		return ABGameObjComp::Type::PHYSICS_CONSTRAINT;
	if(strcmp(str.c_str(), "physicsBody") == 0)
		return ABGameObjComp::Type::PHYSICS_BODY;
	if(strcmp(str.c_str(), "uiText") == 0)
		return ABGameObjComp::Type::UI_TEXT;
	if(strcmp(str.c_str(), "uiProfiler") == 0)
		return ABGameObjComp::Type::UI_PROFILER;
	if(strcmp(str.c_str(), "camera") == 0)
		return ABGameObjComp::Type::CAMERA;
	if(strcmp(str.c_str(), "particleEmitter") == 0)
		return ABGameObjComp::Type::PARTICLE_EMITTER;
	if(strcmp(str.c_str(), "drawable") == 0)
		return ABGameObjComp::Type::DRAWABLE;

	return ABGameObjComp::Type::UNSET;
}