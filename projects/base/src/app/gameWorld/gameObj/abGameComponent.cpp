#include "abGameComponent.h"

ABGameObjComp::ABGameObjComp()
{
	mType = UNSET;
	mId = 0;
	mParentGameObj = NULL;
	mParentGameComp = NULL;

	mMatrix = glm::mat4(1);
	mDerivedMatrix = glm::mat4(1);
}

ABGameObjComp::~ABGameObjComp()
{

}

void ABGameObjComp::setId(const std::vector<std::string>& strList)
{
	mId = calculateId(strList);
}

void ABGameObjComp::setId(const std::string& str)
{
	mId = calculateId(str);
}

unsigned int ABGameObjComp::calculateId(const std::vector<std::string>& strList)
{
	std::string str = strListToStr(strList);

	return calculateId(str);
}

unsigned int ABGameObjComp::calculateId(const std::string& str)
{
	return Utilities::hash(str);
}

std::string ABGameObjComp::strListToStr(const std::vector<std::string>& strList)
{
	std::string str;

	for(int i = 0; i < strList.size(); i++)
	{
		str += strList[i];
	}

	return str;
}

bool ABGameObjComp::compareId(unsigned int idToTest)
{
	return mId == idToTest;
}