#ifndef UI_PROFILER_COMP_H
#define UI_PROFILER_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

#include "app\gameWorld\gameObj\gameComponent\drawableCompMgr.h"

class UiProfilerComp : public ABGameObjComp
{
public:
	UiProfilerComp();
	virtual ~UiProfilerComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);


private:
	DrawableComp* mDrawable;

	int mChartX;
	int mChartY;
	float mChartQuadSize;
	std::vector<float> graphHeight;

	void buildChart();
};

#endif // UI_PROFILER_COMP_H