#include "uiProfilerComp.h"

#include "app/profiler/profile.h"

UiProfilerComp::UiProfilerComp()
{
	mType = Type::UI_PROFILER;
	mParentGameObj = NULL;
}

UiProfilerComp::~UiProfilerComp()
{

}

void UiProfilerComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);
	
	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	// set translation
	//mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	mDrawable = new	DrawableComp();
	DrawableCompMgr::GetInstance()->add(mDrawable);

	mDrawable->setup(nameIdList, component["uiDrawable"], mParentGameObj, this);

	mDrawable->setWinding(GL_CW);

	// change the verts
	std::vector<glm::vec3>& vertices = mDrawable->getMeshData()->getVertices();
	for(int i = 0; i < vertices.size(); i++)
	{
		vertices[i].y = vertices[i].z;
		vertices[i].z = 0;
	}

	mChartX = component["size"]["x"].asInt();
	mChartY = component["size"]["y"].asInt();
	mChartQuadSize = component["size"]["quadSize"].asDouble();
	float startPosY = ((float)mChartY / 2) * -mChartQuadSize;

	graphHeight.resize(Profiler::GetInstance()->getSecondIndexCount());
	for(int i = 0; i < graphHeight.size(); i++)
	{
		graphHeight[i] = startPosY + mChartQuadSize;
	}
}

void UiProfilerComp::eventReceiver(ABEvent* theEvent)
{

}

void UiProfilerComp::tick(const float deltaT)
{
	profile();

	//if(mParentGameComp != NULL)
	//	mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	//else
	//	mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;
	
	buildChart();
}

void UiProfilerComp::buildChart()
{
	Profiler* profiler = Profiler::GetInstance();
	
	float startPosY = ((float)mChartY / 2) * -mChartQuadSize;
	int mSecondIndex = profiler->getSecondIndex();

	graphHeight[mSecondIndex] = startPosY + mChartQuadSize;

	std::vector<TimeData> timeData;
	for (auto it = profiler->getProfileStack().begin(); it != profiler->getProfileStack().end(); ++it)
	{
		it->second.getTime(profiler->getChartDepth(), mSecondIndex, timeData);
	}

	std::vector<glm::vec3>& vertices = mDrawable->getMeshData()->getVertices();
	int graphLevel = 0;
	for(int y = 2; y <= mChartY - 2; y++)
	{

		if(graphLevel < timeData.size())
		{
			graphHeight[mSecondIndex] += (11 * mChartQuadSize) * timeData[graphLevel].time;
		}
		int vert = (mChartX + 1) * y + (mSecondIndex + 1);
		vertices[vert].y = graphHeight[mSecondIndex];
		graphLevel++;
	}

	MeshData* mesh = mDrawable->getMeshData();

	mesh->updateBuffer(vertices, mesh->getVertexBufferId());
}