#include "particleEmitterCompMgr.h"

ParticleEmitterCompMgr* ParticleEmitterCompMgr::mInstance = NULL;

ParticleEmitterCompMgr* ParticleEmitterCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new ParticleEmitterCompMgr();

	return mInstance;
}

ParticleEmitterCompMgr::ParticleEmitterCompMgr()
{
}

ParticleEmitterCompMgr::~ParticleEmitterCompMgr()
{
	mInstance = NULL;
}

ParticleEmitterComp* ParticleEmitterCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	ParticleEmitterComp* comp = new ParticleEmitterComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

ParticleEmitterComp* ParticleEmitterCompMgr::add(ParticleEmitterComp* component)
{
	mCompList.push_back(component);
	return component;
}

ParticleEmitterComp* ParticleEmitterCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

ParticleEmitterComp* ParticleEmitterCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void ParticleEmitterCompMgr::tick(const float deltaT)
{
	profile();
	
	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}
}