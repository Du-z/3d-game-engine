#include "luaScriptComp.h"


LuaScriptComp::LuaScriptComp()
{
	mType = Type::LUA_SCRIPT;
	mParentGameObj = NULL;
}

LuaScriptComp::~LuaScriptComp()
{

}

void LuaScriptComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;
	
	// set translation
	mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;


}

void LuaScriptComp::eventReceiver(ABEvent* theEvent)
{

}

void LuaScriptComp::tick(const float deltaT)
{
	profile();

	if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;
}