#ifndef LUA_SCRIPT_COMP_H
#define LUA_SCRIPT_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

class LuaScriptComp : public ABGameObjComp
{
public:
	LuaScriptComp();
	virtual ~LuaScriptComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

private:
};

#endif // LUA_SCRIPT_COMP_H