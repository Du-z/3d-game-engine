#ifndef PHYSICS_CONSTRAINT_COMPONENT_MGR_H
#define PHYSICS_CONSTRAINT_COMPONENT_MGR_H

#include "physicsContraintCompMgr/physicsConstraintComp.h"

class PhysicsConstraintCompMgr
{
public:
	~PhysicsConstraintCompMgr();
	static PhysicsConstraintCompMgr* GetInstance();

	PhysicsConstraintComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	PhysicsConstraintComp* add(PhysicsConstraintComp* component);

	PhysicsConstraintComp* getComponent(const std::string& str);
	PhysicsConstraintComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	PhysicsConstraintCompMgr();
	static PhysicsConstraintCompMgr* mInstance;

	std::vector<PhysicsConstraintComp*> mCompList;
};

#endif // PHYSICS_CONSTRAINT_COMPONENT_MGR_H