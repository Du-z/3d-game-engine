#include "uiTextComp.h"


UiTextComp::UiTextComp()
{
	mType = Type::LUA_SCRIPT;
	mParentGameObj = NULL;
}

UiTextComp::~UiTextComp()
{

}

void UiTextComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	// set translation
	//mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	mStr = component["string"].asString();
	MeshData* mesh = buildMesh(component["size"].asDouble());
	mMeshName = strListToStr(nameIdList);
	ResMgr::GetInstance()->addMesh(mMeshName, mesh);

	mDrawable = new	DrawableComp();
	DrawableCompMgr::GetInstance()->add(mDrawable);

	mDrawable->setup(nameIdList, component["uiDrawable"], mParentGameObj, this);
	mDrawable->setMeshData(mesh);
}

MeshData* UiTextComp::buildMesh(const float size)
{
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	unsigned int length = mStr.size();
	float offsetY = 0;
	int offsetX = 0;

	// Fill buffers
	for ( unsigned int i = 0, indicies = 0; i < length; i++ ){

		if(mStr[i] == '\n')
		{
			offsetY -= size;
			offsetX = i + 1;
		}
		else
		{
			// Build Verts
			glm::vec3 vertex_up_left((i - offsetX) * size, offsetY + size, 0);
			glm::vec3 vertex_up_right((i - offsetX) * size + size, offsetY + size, 0);
			glm::vec3 vertex_down_right((i - offsetX) * size + size, offsetY, 0);
			glm::vec3 vertex_down_left((i - offsetX) * size, offsetY, 0);

			vertices.push_back(vertex_up_left);
			vertices.push_back(vertex_down_left);
			vertices.push_back(vertex_up_right);

			vertices.push_back(vertex_down_right);
			vertices.push_back(vertex_up_right);
			vertices.push_back(vertex_down_left);

			// Build UV
			char character = mStr[i];
			float uvX = (character%16)/16.0f;
			float uvY = (character/16)/16.0f;

			glm::vec2 uv_up_left(uvX, 1.0f - uvY);
			glm::vec2 uv_up_right(uvX+1.0f/16.0f, 1.0f - uvY);
			glm::vec2 uv_down_right(uvX+1.0f/16.0f, 1.0f - (uvY + 1.0f/16.0f));
			glm::vec2 uv_down_left(uvX, 1.0f - (uvY + 1.0f/16.0f));

			uvs.push_back(uv_up_left);
			uvs.push_back(uv_down_left);
			uvs.push_back(uv_up_right);

			uvs.push_back(uv_down_right);
			uvs.push_back(uv_up_right);
			uvs.push_back(uv_down_left);

			// Build Normals
			glm::vec3 norm(0, 1, 0);

			normals.push_back(norm);
			normals.push_back(norm);
			normals.push_back(norm);

			normals.push_back(norm);
			normals.push_back(norm);
			normals.push_back(norm);

			// Build Indicies
			indices.push_back(indicies++);
			indices.push_back(indicies++);
			indices.push_back(indicies++);

			indices.push_back(indicies++);
			indices.push_back(indicies++);
			indices.push_back(indicies++);
		}
	}


	MeshData* mesh = new MeshData();

	mesh->create(indices, vertices, uvs, normals);

	return mesh;
}

void UiTextComp::eventReceiver(ABEvent* theEvent)
{

}

void UiTextComp::tick(const float deltaT)
{
	profile();

	/*if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;*/

}