#ifndef UI_TEXT_COMP_H
#define UI_TEXT_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

#include "app\gameWorld\gameObj\gameComponent\drawableCompMgr.h"

class UiTextComp : public ABGameObjComp
{
public:
	UiTextComp();
	virtual ~UiTextComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	MeshData* buildMesh(const float size);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

private:
	PROP_GR_S(std::string, mStr, String);

	std::string mMeshName;

	DrawableComp* mDrawable;
};

#endif // UI_TEXT_COMP_H