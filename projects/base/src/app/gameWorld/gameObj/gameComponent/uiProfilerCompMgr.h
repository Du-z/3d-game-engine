#ifndef UI_PROFILER_COMPONENT_MGR_H
#define UI_PROFILER_COMPONENT_MGR_H

#include "uiProfilerCompMgr/uiProfilerComp.h"

class UiProfilerCompMgr
{
public:
	~UiProfilerCompMgr();
	static UiProfilerCompMgr* GetInstance();

	UiProfilerComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	UiProfilerComp* add(UiProfilerComp* component);

	UiProfilerComp* getComponent(const std::string& str);
	UiProfilerComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	UiProfilerCompMgr();
	static UiProfilerCompMgr* mInstance;

	std::vector<UiProfilerComp*> mCompList;
};

#endif // UI_PROFILER_COMPONENT_MGR_H