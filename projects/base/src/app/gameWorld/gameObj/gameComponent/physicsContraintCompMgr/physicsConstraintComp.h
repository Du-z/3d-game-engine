#ifndef PHYSICS_CONSTRAINT_COMP_H
#define PHYSICS_CONSTRAINT_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

#include "btBulletDynamicsCommon.h"

#include "BulletCollision/CollisionShapes/btShapeHull.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

class PhysicsConstraintComp : public ABGameObjComp
{
public:
	PhysicsConstraintComp();
	virtual ~PhysicsConstraintComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

private:
	btTypedConstraint* mConstraint;

	btHingeConstraint* createHinge(btRigidBody* bodyA, btRigidBody* bodyB, const Json::Value& component);
	btPoint2PointConstraint* createBallSocket(btRigidBody* bodyA, btRigidBody* bodyB, const Json::Value& component);
};

#endif // PHYSICS_CONSTRAINT_COMP_H