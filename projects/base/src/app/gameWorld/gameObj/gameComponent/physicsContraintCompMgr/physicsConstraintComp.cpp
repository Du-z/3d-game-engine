#include "physicsConstraintComp.h"

#include "app/gameWorld/gameObj/gameComponent/physicsBodyCompMgr.h"

#include "app.h"

PhysicsConstraintComp::PhysicsConstraintComp()
{
	mType = Type::DRAWABLE;
	mParentGameObj = NULL;
}

PhysicsConstraintComp::~PhysicsConstraintComp()
{
	
}

void PhysicsConstraintComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	// set translation
	mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	PhysicsBodyComp* compA = PhysicsBodyCompMgr::GetInstance()->getComponent(nameIdList[0] + component["bodyA"].asString());
	PhysicsBodyComp* compB = PhysicsBodyCompMgr::GetInstance()->getComponent(nameIdList[0] + component["bodyB"].asString());

	if(compA == NULL)
	{
		logWarn("could not create '" << strListToStr(nameIdList) << "', '" << nameIdList[0] << component["bodyA"].asString() << "' is not a valid physics body.");
		return;
	}
	if(compB == NULL)
	{
		logWarn("could not create '" << strListToStr(nameIdList) << "', '" << nameIdList[0] << component["bodyB"].asString() << "' is not a valid physics body.");
		return;
	}

	btRigidBody* bodyA = compA->getRigidBody();
	btRigidBody* bodyB = compB->getRigidBody();

	// Make the constraint
	mConstraint = NULL;
	std::string str = component["constraintType"].asString();
	if(strcmp(str.c_str(), "hinge") == 0)
		mConstraint = createHinge(bodyA, bodyB, component);
	else if(strcmp(str.c_str(), "ballSocket") == 0)
		mConstraint = createBallSocket(bodyA, bodyB, component);
	else
		logWarn("'" << str << "' is not a valid physics constraint type." );

	btDiscreteDynamicsWorld* dynamicsWorld = App::GetInstance()->getDynamicsWorld();
	dynamicsWorld->addConstraint(mConstraint);

	bodyA->addConstraintRef(mConstraint);
	bodyB->addConstraintRef(mConstraint);
}

btPoint2PointConstraint* PhysicsConstraintComp::createBallSocket(btRigidBody* bodyA, btRigidBody* bodyB, const Json::Value& component)
{	
	// add some data to build constraint frames
	btVector3 pivotA(component["pivotA"]["x"].asDouble(), component["pivotA"]["y"].asDouble(), component["pivotA"]["z"].asDouble());
	btVector3 pivotB(component["pivotB"]["x"].asDouble(), component["pivotB"]["y"].asDouble(), component["pivotB"]["z"].asDouble());

	return new btPoint2PointConstraint(*bodyA, *bodyB, pivotA, pivotB);
}

btHingeConstraint* PhysicsConstraintComp::createHinge(btRigidBody* bodyA, btRigidBody* bodyB, const Json::Value& component)
{	
	// add some data to build constraint frames
	btVector3 pivotA(component["pivotA"]["x"].asDouble(), component["pivotA"]["y"].asDouble(), component["pivotA"]["z"].asDouble());
	btVector3 pivotB(component["pivotB"]["x"].asDouble(), component["pivotB"]["y"].asDouble(), component["pivotB"]["z"].asDouble());
	btVector3 axisA(component["axisA"]["x"].asDouble(), component["axisA"]["y"].asDouble(), component["axisA"]["z"].asDouble()); 
	btVector3 axisB(component["axisB"]["x"].asDouble(), component["axisB"]["y"].asDouble(), component["axisB"]["z"].asDouble());

	return new btHingeConstraint(*bodyA, *bodyB, pivotA, pivotB, axisA, axisB);
}

void PhysicsConstraintComp::eventReceiver(ABEvent* theEvent)
{

}

void PhysicsConstraintComp::tick(const float deltaT)
{
	profile();

	if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;
}