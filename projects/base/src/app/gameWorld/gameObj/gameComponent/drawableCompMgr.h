#ifndef DRAWABLE_COMPONENT_MGR_H
#define DRAWABLE_COMPONENT_MGR_H

#include "drawableCompMgr/drawableComp.h"

#include "app/bulletDebugger.h"

class DrawableCompMgr
{
public:
	~DrawableCompMgr();
	static DrawableCompMgr* GetInstance();

	DrawableComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	DrawableComp* add(DrawableComp* component);

	DrawableComp* getComponent(const std::string& str);
	DrawableComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	DrawableCompMgr();
	static DrawableCompMgr* mInstance;

	void drawBulletDebug();

	void sort();

	BulletDebugDrawer mDebugDrawer;

	std::vector<DrawableComp*> mCompList;

	PROP_G_S(bool, mDrawWireFrame, DrawWireFrame);
	PROP_G_S(bool, mDrawBulletDebug, DrawBulletDebug);
};

#endif // DRAWABLE_COMPONENT_MGR_H