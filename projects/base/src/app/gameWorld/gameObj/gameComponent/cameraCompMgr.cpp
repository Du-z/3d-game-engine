#include "CameraCompMgr.h"

CameraCompMgr* CameraCompMgr::mInstance = NULL;

CameraCompMgr* CameraCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new CameraCompMgr();

	return mInstance;
}

CameraCompMgr::CameraCompMgr()
{
	mActiveCamera = NULL;
}

CameraCompMgr::~CameraCompMgr()
{
	mInstance = NULL;
}

CameraComp* CameraCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	CameraComp* comp = new CameraComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

CameraComp* CameraCompMgr::add(CameraComp* component)
{
	if(mActiveCamera == NULL)
		mActiveCamera = component;
	
	mCompList.push_back(component);

	return component;
}

CameraComp* CameraCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

CameraComp* CameraCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void CameraCompMgr::tick(const float deltaT)
{
	profile();
	
	// tick for the active camera only
	mActiveCamera->tick(deltaT);
}