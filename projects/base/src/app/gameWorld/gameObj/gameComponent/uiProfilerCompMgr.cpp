#include "uiProfilerCompMgr.h"

UiProfilerCompMgr* UiProfilerCompMgr::mInstance = NULL;

UiProfilerCompMgr* UiProfilerCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new UiProfilerCompMgr();

	return mInstance;
}

UiProfilerCompMgr::UiProfilerCompMgr()
{
}

UiProfilerCompMgr::~UiProfilerCompMgr()
{
	mInstance = NULL;
}

UiProfilerComp* UiProfilerCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	UiProfilerComp* comp = new UiProfilerComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

UiProfilerComp* UiProfilerCompMgr::add(UiProfilerComp* component)
{
	mCompList.push_back(component);
	return component;
}

UiProfilerComp* UiProfilerCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

UiProfilerComp* UiProfilerCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void UiProfilerCompMgr::tick(const float deltaT)
{
	profile();
	
	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}
}