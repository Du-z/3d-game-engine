#include "luaScriptCompMgr.h"

LuaScriptCompMgr* LuaScriptCompMgr::mInstance = NULL;

LuaScriptCompMgr* LuaScriptCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new LuaScriptCompMgr();

	return mInstance;
}

LuaScriptCompMgr::LuaScriptCompMgr()
{
}

LuaScriptCompMgr::~LuaScriptCompMgr()
{
	mInstance = NULL;
}

LuaScriptComp* LuaScriptCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	LuaScriptComp* comp = new LuaScriptComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

LuaScriptComp* LuaScriptCompMgr::add(LuaScriptComp* component)
{
	mCompList.push_back(component);
	return component;
}

LuaScriptComp* LuaScriptCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

LuaScriptComp* LuaScriptCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void LuaScriptCompMgr::tick(const float deltaT)
{
	profile();
	
	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}
}