#ifndef PHYSICS_BODY_COMPONENT_H
#define PHYSICS_BODY_COMPONENT_H

#include "app\gameWorld\gameObj\abGameComponent.h"

#include "btBulletDynamicsCommon.h"

#include "BulletCollision/CollisionShapes/btShapeHull.h"
#include "BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h"

class PhysicsBodyComp : public ABGameObjComp
{
public:
	PhysicsBodyComp();
	virtual ~PhysicsBodyComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);
private:

	MeshData* getMesh(const Json::Value& component);

	btBoxShape* createBox(const btVector3& halfExtent);
	btSphereShape* createSphere(const btScalar radius);
	btCylinderShape* createCylinder(const btVector3& halfExtent);
	btConeShape* createCone(const btScalar radius, const btScalar height);
	btCapsuleShape* createCapsule(const btScalar radius, const btScalar height);
	btConvexHullShape* meshToConvexHull(MeshData* mesh);
	btHeightfieldTerrainShape* meshToHeightfieldTerrain(MeshData* mesh);

	btCollisionShape* mCollisionShape;
	PROP_G(btRigidBody*, mRigidBody, RigidBody);

	PROP_G(bool, mPlayerControled, playerControled);
	btVector3 mVelocityScaler;
	btVector3 mAngularVelocity;
};

#endif // PHYSICS_BODY_COMPONENT_H