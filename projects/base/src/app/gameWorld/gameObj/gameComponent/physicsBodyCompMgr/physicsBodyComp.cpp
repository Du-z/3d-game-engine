#include "physicsBodyComp.h"

#include "LinearMath/btQuickprof.h"
#include "LinearMath/btIDebugDraw.h"
#include "LinearMath/btGeometryUtil.h"

#include "app.h"

#include "app\gameWorld\gameObj\gameComponentMgr.h"

PhysicsBodyComp::PhysicsBodyComp()
{
	mType = Type::DRAWABLE;
	mParentGameObj = NULL;

	mVelocityScaler = btVector3(0, 0, 0);
	mAngularVelocity = btVector3(0, 0, 0);
}

PhysicsBodyComp::~PhysicsBodyComp()
{

}

void PhysicsBodyComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	// set translation
	mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	// Make body shape
	mCollisionShape = NULL;
	std::string str = component["bodyType"].asString();
	if(strcmp(str.c_str(), "box") == 0)
		mCollisionShape = createBox(btVector3(component["size"]["x"].asDouble(), component["size"]["y"].asDouble(), component["size"]["z"].asDouble()));
	else if(strcmp(str.c_str(), "sphere") == 0)
		mCollisionShape = createSphere(component["radius"].asDouble());
	else if(strcmp(str.c_str(), "cylinder") == 0)
		mCollisionShape = createCylinder(btVector3(component["size"]["x"].asDouble(), component["size"]["y"].asDouble(), component["size"]["z"].asDouble()));
	else if(strcmp(str.c_str(), "cone") == 0)
		mCollisionShape = createCone(component["radius"].asDouble(), component["height"].asDouble());
	else if(strcmp(str.c_str(), "capsule") == 0)
		mCollisionShape = createCapsule(component["radius"].asDouble(), component["height"].asDouble());
	else if(strcmp(str.c_str(), "convexHull") == 0)
		mCollisionShape = meshToConvexHull(getMesh(component));
	else if(strcmp(str.c_str(), "heightfieldTerrain") == 0)
		mCollisionShape = meshToHeightfieldTerrain(getMesh(component));
	else
		logWarn("'" << str << "' is not a valid physics collision shape." );

	btScalar mass = 0;
	
	// Set collision type
	int collisionType = 0;
	str = component["collisionType"].asString(); 
	if(strcmp(str.c_str(), "dynamic") == 0)
	{
		collisionType = 0;
		mass = 1; 
	}
	else if(strcmp(str.c_str(), "static") == 0)
	{
		collisionType = btCollisionObject::CF_STATIC_OBJECT;
	}
	else if(strcmp(str.c_str(), "kinematic") == 0)
	{
		collisionType = btCollisionObject::CF_KINEMATIC_OBJECT;
	}
	else
	{
		logWarn("'" << str << "' is not a valid physics collision type." );
	}

	glm::vec3 pos = glm::vec3(mDerivedMatrix[3]);

	btDefaultMotionState* fallMotionState = new btDefaultMotionState(btTransform(btQuaternion(0,0,0,1), btVector3(pos.x, pos.y, pos.z)));
	btVector3 fallInertia(0,0,0);
	mCollisionShape->calculateLocalInertia(mass,fallInertia);
	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, fallMotionState, mCollisionShape, fallInertia);
	mRigidBody = new btRigidBody(fallRigidBodyCI);
	mRigidBody->setCollisionFlags(mRigidBody->getCollisionFlags() | collisionType);

	mRigidBody->setSleepingThresholds(0.5f, 0.5f);

	mRigidBody->setUserPointer(this);

	btVector3 angularfactor(1, 1, 1);
	if(component["lockRotation"]["x"].asBool())
		angularfactor.setX(0);
	if(component["lockRotation"]["y"].asBool())
		angularfactor.setY(0);
	if(component["lockRotation"]["z"].asBool())
		angularfactor.setZ(0);
	mRigidBody->setAngularFactor(angularfactor);

	btDiscreteDynamicsWorld* dynamicsWorld = App::GetInstance()->getDynamicsWorld();
	dynamicsWorld->addRigidBody(mRigidBody);

	mPlayerControled = component["playerControled"].asBool();
	if(mPlayerControled)
	{
		EventMgr::GetInstance()->addSubscriber(Event::KEY_PRESSED, this);
		EventMgr::GetInstance()->addSubscriber(Event::KEY_RELEASED, this);
		EventMgr::GetInstance()->addSubscriber(Event::CURSOR_MOVED, this);

		mRigidBody->setActivationState(DISABLE_DEACTIVATION);
	}
}

btBoxShape* PhysicsBodyComp::createBox(const btVector3& halfExtent)
{
	return new btBoxShape(halfExtent);
}

btCylinderShape* PhysicsBodyComp::createCylinder(const btVector3& halfExtent)
{
	return new btCylinderShape(halfExtent);
}

btSphereShape* PhysicsBodyComp::createSphere(const btScalar radius)
{
	return new btSphereShape(radius);
}

btCapsuleShape* PhysicsBodyComp::createCapsule(const btScalar radius, const btScalar height)
{
	return new btCapsuleShape(radius, height);
}

btConeShape* PhysicsBodyComp::createCone(const btScalar radius, const btScalar height)
{
	return new btConeShape(radius, height);
}

btHeightfieldTerrainShape* PhysicsBodyComp::meshToHeightfieldTerrain(MeshData* mesh)
{

	float maxHeight = mesh->getVertices()[0].y;

	int meshSize = mesh->getVertices().size();
	float *terrainHeights = new float[meshSize]; 
	for(int i = 0; i < meshSize; i++)
	{
		terrainHeights[i] = mesh->getVertices()[i].y;

		if(mesh->getVertices()[i].y > maxHeight)
			maxHeight =  mesh->getVertices()[i].y;
	}

	float minHeight = -maxHeight;
	int upAxis = 1;
	
	btHeightfieldTerrainShape* heightfieldShape = new btHeightfieldTerrainShape(mesh->getMeshX() + 1, mesh->getMeshZ() + 1, terrainHeights, 1/*ignore*/, minHeight, maxHeight, upAxis, PHY_FLOAT, false);

	heightfieldShape->setLocalScaling(btVector3(2.0f, 1.0f, 2.0f));

	return heightfieldShape;
}

btConvexHullShape* PhysicsBodyComp::meshToConvexHull(MeshData* mesh)
{
	btConvexHullShape* convexShape = new btConvexHullShape();
	for(int i = 0; i < mesh->getIndicesCount(); i++)
	{
		int indicies = mesh->getIndices()[i+0];

		btVector3 vertex0(mesh->getVertices()[indicies].x, mesh->getVertices()[indicies].y, mesh->getVertices()[indicies].z);

		convexShape->addPoint(vertex0);	
	}

	return convexShape;
}

MeshData* PhysicsBodyComp::getMesh(const Json::Value& component)
{
	// Grab the mesh
	std::string meshDir = component["mesh"].asString();
	if(meshDir.length() != 0)
	{
		return ResMgr::GetInstance()->getMesh(meshDir); 
	}
	else
	{
		Json::Value meshJsonData = component["meshGen"];

		return ResMgr::GetInstance()->genMesh(meshJsonData["name"].asString(),
			meshJsonData["x"].asInt(), meshJsonData["y"].asInt(), meshJsonData["quadSize"].asDouble(),
			meshJsonData["maxU"].asDouble(), meshJsonData["maxV"].asDouble(),
			meshJsonData["randY"].asDouble(), meshJsonData["smoothPasses"].asInt()
			);
	}
}

void PhysicsBodyComp::eventReceiver(ABEvent* theEvent)
{
	// Check the event type
	if(theEvent->getEventType() == Event::CURSOR_MOVED)
	{
		CursorMoved* cursor = static_cast<CursorMoved*>(theEvent);
		mAngularVelocity.setY(cursor->getDeltaXPos());
	}
	else if(theEvent->getEventType() == Event::KEY_PRESSED)
	{
		KeyPressed* key = static_cast<KeyPressed*>(theEvent);
		switch (key->getKey())
		{
		case GLFW_KEY_W:
			mVelocityScaler.setZ(-1);
			break;
		case GLFW_KEY_S:
			mVelocityScaler.setZ(1);
			break;
		case GLFW_KEY_A:
			mVelocityScaler.setX(-1);
			break;
		case GLFW_KEY_D:
			mVelocityScaler.setX(1);
			break;
		default:
			return;
		}

	}
	else if(theEvent->getEventType() == Event::KEY_RELEASED)
	{
		KeyReleased* key = static_cast<KeyReleased*>(theEvent);
		switch (key->getKey())
		{
		case GLFW_KEY_W:
			mVelocityScaler.setZ(0);
			break;
		case GLFW_KEY_S:
			mVelocityScaler.setZ(0);
			break;
		case GLFW_KEY_A:
			mVelocityScaler.setX(0);
			break;
		case GLFW_KEY_D:
			mVelocityScaler.setX(0);
			break;
		default:
			return;
		}

	}
}

void PhysicsBodyComp::tick(const float deltaT)
{
	profile();

	if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;

	btTransform trans;
	mRigidBody->getMotionState()->getWorldTransform(trans);

	trans.getOpenGLMatrix(&mDerivedMatrix[0][0]);

	// Need to set the local

	if(mPlayerControled)
	{
		btVector3 impulseToApply(0, 0, 0);
		btVector3 maxVelocity(10, 10, 10);
		btVector3 impulsePerSec(20, 20, 20);

		impulseToApply = impulsePerSec * mVelocityScaler * deltaT;

		btVector3 curVelocity = mRigidBody->getLinearVelocity();
		if(curVelocity.getZ() > maxVelocity.getZ() || curVelocity.getZ() < -maxVelocity.getZ())
			impulseToApply.setZ(0);
		if(curVelocity.getX() > maxVelocity.getX() || curVelocity.getX() < -maxVelocity.getX())
			impulseToApply.setX(0);


		btMatrix3x3& bodyRot = mRigidBody->getWorldTransform().getBasis();
		btVector3 correctedImpulse = bodyRot * impulseToApply;

		mRigidBody->applyCentralImpulse(correctedImpulse);


		mRigidBody->setAngularVelocity(-mAngularVelocity);
		mAngularVelocity = btVector3(0, 0, 0);
	}
}

//dynamicsWorld->removeRigidBody(mRigidBody);
//delete mRigidBody->getMotionState();
//delete mRigidBody;
//delete mCollisionShape;