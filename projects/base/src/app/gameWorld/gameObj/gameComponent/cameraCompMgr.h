#ifndef CAMERA_COMPONENT_MGR_H
#define CAMERA_COMPONENT_MGR_H

#include "cameraCompMgr/cameraComp.h"

class CameraCompMgr
{
public:
	~CameraCompMgr();
	static CameraCompMgr* GetInstance();

	CameraComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	CameraComp* add(CameraComp* component);

	CameraComp* getComponent(const std::string& str);
	CameraComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	CameraCompMgr();
	static CameraCompMgr* mInstance;

	PROP_G_S(CameraComp*, mActiveCamera, ActiveCamera);

	std::vector<CameraComp*> mCompList;
};

#endif // CAMERA_COMPONENT_MGR_H