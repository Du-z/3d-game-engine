#include "PhysicsConstraintCompMgr.h"

PhysicsConstraintCompMgr* PhysicsConstraintCompMgr::mInstance = NULL;

PhysicsConstraintCompMgr* PhysicsConstraintCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new PhysicsConstraintCompMgr();

	return mInstance;
}

PhysicsConstraintCompMgr::PhysicsConstraintCompMgr()
{
}

PhysicsConstraintCompMgr::~PhysicsConstraintCompMgr()
{
	mInstance = NULL;
}

PhysicsConstraintComp* PhysicsConstraintCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	PhysicsConstraintComp* comp = new PhysicsConstraintComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp); 

	return comp;
}

PhysicsConstraintComp* PhysicsConstraintCompMgr::add(PhysicsConstraintComp* component)
{
	mCompList.push_back(component);
	return component;
}

PhysicsConstraintComp* PhysicsConstraintCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

PhysicsConstraintComp* PhysicsConstraintCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void PhysicsConstraintCompMgr::tick(const float deltaT)
{
	profile();
	
	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}
}