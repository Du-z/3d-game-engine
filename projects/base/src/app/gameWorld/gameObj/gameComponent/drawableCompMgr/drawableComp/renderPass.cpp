#include "renderPass.h"

#include "app\logger.h"
#include "app\gameWorld\gameObj\gameComponent\drawableCompMgr\drawableComp.h"

RenderPass::RenderPass()
{
	mParentDrawable = NULL;

	mRunCount = 1;
	
	mShaderProg = NULL;
	
	mBlendModeSrc = GL_SRC_ALPHA;
	mBlendModeDst = GL_ONE_MINUS_SRC_ALPHA;
}

RenderPass::~RenderPass()
{

}

void RenderPass::setup(const Json::Value& renderPass, DrawableComp* parentDrawable)
{
	mParentDrawable = parentDrawable;
	
	mShaderProg = ResMgr::GetInstance()->getShader(renderPass["vertShader"].asString(), renderPass["fragShader"].asString());

	mRunCount = renderPass["runCount"].asInt(); 

	std::string val = renderPass["blendMode"]["src"].asString();
	if(val.length() != 0)
		mBlendModeSrc = strToBlendMode(val);
	val = renderPass["blendMode"]["dst"].asString();
	if(val.length() != 0)
		mBlendModeDst = strToBlendMode(val);
}

void RenderPass::draw()
{
	profile();

	MeshData* mesh = mParentDrawable->getMeshData();
	
	// Bind VAO
	// GL_INVALID_OPERATION is generated if array is not zero or the name of a vertex array object previously returned from a call to glGenVertexArrays.
	glBindVertexArray(mesh->getVertexArrayId());

	// Draw the triangles !
	glDrawElements(
		GL_TRIANGLES, // mode
		mesh->getIndicesCount(), // count
		GL_UNSIGNED_SHORT, // type
		(void*)0 // element array buffer offset
		);

	// Unbind VAO (make sure nothing else tries to modify it)
	glBindVertexArray(0);
}

void RenderPass::preDrawSetup(CameraComp* camera, const glm::mat4& modelMatrix)
{
	profile();

	glUseProgram(mShaderProg->ProgramId);

	glBlendFunc(mBlendModeSrc, mBlendModeDst);
	
	const GLuint knownBadValue = -1;

	if(mShaderProg->MvpId != knownBadValue) // only do this if the shader has this uniform
	{
		glm::mat4 MVP = camera->getProjectionMatrix() * camera->getViewMatrix() * modelMatrix;
		glUniformMatrix4fv(mShaderProg->MvpId, 1, GL_FALSE, &MVP[0][0]);
	}

	if(mShaderProg->ProjectionMatrixId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniformMatrix4fv(mShaderProg->ProjectionMatrixId, 1, GL_FALSE, &camera->getProjectionMatrix()[0][0]);
	}

	if(mShaderProg->ModelMatrixId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniformMatrix4fv(mShaderProg->ModelMatrixId, 1, GL_FALSE, &modelMatrix[0][0]);
	}

	if(mShaderProg->ViewMatrixId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniformMatrix4fv(mShaderProg->ViewMatrixId, 1, GL_FALSE, &camera->getViewMatrix()[0][0]);
	}

	if(mShaderProg->MvInvTransMatrixId != knownBadValue) // only do this if the shader has this uniform
	{
		glm::mat3 invTransMatrix = glm::mat3(camera->getViewMatrix() * modelMatrix);
		invTransMatrix = glm::inverse(invTransMatrix);
		glUniformMatrix3fv(mShaderProg->MvInvTransMatrixId, 1, GL_TRUE, &invTransMatrix[0][0]);
	}

	if(mShaderProg->CameraPosId != knownBadValue) // only do this if the shader has this uniform
	{
		glm::vec3 pos = glm::vec3(camera->getDerivedMatrix()[3]);
		glUniform3fv(mShaderProg->CameraPosId, 3, &pos[0]);
	}

	if(mShaderProg->AmbientTintId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniform4f(mShaderProg->AmbientTintId, 0.1, 0.1, 0.1, 1);
	}

	if(mShaderProg->LightPosId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniform3f(mShaderProg->LightPosId, 0, 4, 0);
	}

	if(mShaderProg->SpecularPowerId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniform1f(mShaderProg->SpecularPowerId, 25.5f);
	}

	if(mShaderProg->SpecularTintId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniform4f(mShaderProg->SpecularTintId, 1.f, 1.f, 1.f, 1.f);
	}

	if(mShaderProg->DiffuseTintId != knownBadValue) // only do this if the shader has this uniform
	{
		glUniform4f(mShaderProg->DiffuseTintId, 1.f, 1.f, 1.f, 1.f);
	}

	GLuint texture;
	
	texture = mParentDrawable->getDiffuseTexture().getTextureId();
	if(texture != 0 && mShaderProg->DiffuseTextureId != knownBadValue)
	{
		GLuint textureUnit = 0;
		// Set our texture sampler to use Texture Unit textureUnit
		glUniform1i(mShaderProg->DiffuseTextureId, textureUnit); 

		// Bind our texture in Texture Unit textureUnit
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	texture = mParentDrawable->getEmissiveTexture().getTextureId();
	if(texture != 0 && mShaderProg->EmissiveTextureId != knownBadValue)
	{
		GLuint textureUnit = 1;
		// Set our texture sampler to use Texture Unit textureUnit
		glUniform1i(mShaderProg->EmissiveTextureId, textureUnit); 

		// Bind our texture in Texture Unit textureUnit
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	texture = mParentDrawable->getNormalTexture().getTextureId();
	if(texture != 0 && mShaderProg->NormalTextureId != knownBadValue)
	{
		GLuint textureUnit = 1;
		// Set our texture sampler to use Texture Unit textureUnit
		glUniform1i(mShaderProg->NormalTextureId, textureUnit); 

		// Bind our texture in Texture Unit textureUnit
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	texture = mParentDrawable->getSpecularTexture().getTextureId();
	if(texture != 0 && mShaderProg->SpecularTextureId != knownBadValue)
	{
		GLuint textureUnit = 2;
		// Set our texture sampler to use Texture Unit textureUnit
		glUniform1i(mShaderProg->SpecularTextureId, textureUnit); 

		// Bind our texture in Texture Unit textureUnit
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(GL_TEXTURE_2D, texture);
	}

}

GLint RenderPass::strToBlendMode(const std::string& str)
{
	if(strcmp(str.c_str(), "zero") == 0)
		return GL_ZERO;
	if(strcmp(str.c_str(), "one") == 0)
		return GL_ONE;
	if(strcmp(str.c_str(), "srcAlpha") == 0)
		return GL_SRC_ALPHA;
	if(strcmp(str.c_str(), "invSrcAlpha") == 0)
		return GL_ONE_MINUS_SRC_ALPHA;
	if(strcmp(str.c_str(), "dstColour") == 0)
		return GL_DST_COLOR;

	logWarn("Invalid Blend Mode: " << str);
	return GL_SRC_ALPHA;
}