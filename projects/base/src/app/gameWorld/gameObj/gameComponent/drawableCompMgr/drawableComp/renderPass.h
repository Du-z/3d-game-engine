#ifndef RENDER_PASS_H
#define RENDER_PASS_H

// Include GLEW first
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>

#include "app\utilities.h"
#include "app\resMgr.h"

#include "app\gameWorld\gameObj\gameComponent\cameraCompMgr.h"

class DrawableComp;

class RenderPass
{
public:
	RenderPass();
	~RenderPass();

	void setup(const Json::Value& renderPass, DrawableComp* parentDrawable);

	void draw();
	void preDrawSetup(CameraComp* camera, const glm::mat4& modelMatrix);

	void setBlendModeSrc(const std::string& str){mBlendModeSrc = strToBlendMode(str);}
	void setBlendModeDst(const std::string& str){mBlendModeDst = strToBlendMode(str);}

private:
	PROP_S(ShaderData*, mShaderProg, ShaderProg);

	PROP_G_S(GLint, mBlendModeSrc, BlendModeSrc);
	PROP_G_S(GLint, mBlendModeDst, BlendModeDst);

	PROP_G(int, mRunCount, RunCount);

	DrawableComp* mParentDrawable;

	GLint strToBlendMode(const std::string& str);
};

#endif // RENDER_PASS_H