#include "drawableComp.h"

DrawableComp::DrawableComp()
{
	mType = Type::DRAWABLE;
	mParentGameObj = NULL;

	mMesh = NULL;

	mIsTranparent = false;

	mWinding = GL_CCW;
}

DrawableComp::~DrawableComp()
{

}

void DrawableComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	// set translation
	mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	std::string meshDir = component["mesh"].asString();
	Json::Value meshJsonData = component["meshGen"];
	if(meshDir.length() != 0)
	{
		mMesh = ResMgr::GetInstance()->getMesh(meshDir);
	}
	else if(meshJsonData["name"].asString().length() != 0)
	{		
		mMesh = new MeshData; // TODO clean up

		mMesh = ResMgr::GetInstance()->genMesh(meshJsonData["name"].asString(),
			meshJsonData["x"].asInt(), meshJsonData["y"].asInt(), meshJsonData["quadSize"].asDouble(),
			meshJsonData["maxU"].asDouble(), meshJsonData["maxV"].asDouble(),
			meshJsonData["randY"].asDouble(), meshJsonData["smoothPasses"].asInt()
			);
	}
	else
	{
		mMesh = NULL;
	}

	mDiffuseTexture.setup(component["diffuse"]);
	mNormalTexture.setup(component["normal"]);
	mSpecularTexture.setup(component["specular"]);
	mEmissiveTexture.setup(component["emissive"]);

	Json::Value renderPassList = component["passes"];
	for(size_t i = 0; i < renderPassList.size(); ++i)
	{
		RenderPass renderPass;
		renderPass.setup(renderPassList[i], this);
		mRenderPassList.push_back(renderPass);
	}

	mIsTranparent = component["isTransparent"].asBool();
	mDisableZBuffer = component["disableZBuffer"].asBool();
	mDisableRotation = component["disableRotation"].asBool();
}

void DrawableComp::eventReceiver(ABEvent* theEvent)
{

}

void DrawableComp::tick(const float deltaT)
{
	profile();


	if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;

	if(mDisableRotation)
	{
		glm::mat4 tempMat = mDerivedMatrix;
		mDerivedMatrix = glm::mat4(1);
		mDerivedMatrix[3] = tempMat[3];
	}

	glFrontFace(mWinding);

	if(mDisableZBuffer)
		glDisable(GL_DEPTH_TEST); 
	else
		glEnable(GL_DEPTH_TEST);
	
	for(int i = 0; i < mRenderPassList.size(); ++i)
	{
		for(int j = 0; j < mRenderPassList[i].getRunCount(); j++)
		{
			mRenderPassList[i].preDrawSetup(CameraCompMgr::GetInstance()->getActiveCamera(), mDerivedMatrix);

			// Set filter mode
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mDiffuseTexture.getMagFilter());
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mDiffuseTexture.getMinFilter());
			
			// Set wrap mode
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, mDiffuseTexture.getWrapModeX());
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, mDiffuseTexture.getWrapModeY());

			mRenderPassList[i].draw();
		}
	}
}