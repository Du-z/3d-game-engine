#include "textureProperties.h"

#include "app\logger.h"

TextureProps::TextureProps()
{
	mTextureId = 0;

	mMagFilter = GL_NEAREST;
	mMinFilter = GL_NEAREST;

	mWrapModeX = GL_REPEAT; // GL_CLAMP_TO_EDGE 
	mWrapModeY = GL_REPEAT;
}

TextureProps::~TextureProps()
{

}

void TextureProps::setup(const Json::Value& textureProps)
{
	std::string val = textureProps["dir"].asString();
	if(val.length() == 0)
		return;

	mTextureId = ResMgr::GetInstance()->getTexture(val, textureProps["useMipMaps"].asBool());

	val = textureProps["filterMode"]["mag"].asString();
	if(val.length() != 0)	
		mMagFilter = strToFilterMode(val);
	val = textureProps["filterMode"]["min"].asString();
	if(val.length() != 0)	
		mMinFilter = strToFilterMode(val);

	val = textureProps["wrapMode"]["x"].asString();
	if(val.length() != 0)	
		mWrapModeX = strToWrapMode(val);
	val = textureProps["wrapMode"]["y"].asString();
	if(val.length() != 0)
		mWrapModeY = strToWrapMode(val);
}

GLint TextureProps::strToFilterMode(const std::string& str)
{
	if(strcmp(str.c_str(), "N") == 0)
		return GL_NEAREST;
	if(strcmp(str.c_str(), "L") == 0)
		return GL_LINEAR;
	if(strcmp(str.c_str(), "NMN") == 0)
		return GL_NEAREST_MIPMAP_NEAREST;
	if(strcmp(str.c_str(), "LMN") == 0)
		return GL_LINEAR_MIPMAP_NEAREST;
	if(strcmp(str.c_str(), "NML") == 0)
		return GL_NEAREST_MIPMAP_LINEAR;
	if(strcmp(str.c_str(), "LML") == 0)
		return GL_LINEAR_MIPMAP_LINEAR;

	logWarn("Invalid Filter Mode: " << str);
	return GL_NEAREST;
}

GLint TextureProps::strToWrapMode(const std::string& str)
{
	if(strcmp(str.c_str(), "repeat") == 0)
		return GL_REPEAT;
	if(strcmp(str.c_str(), "clamp") == 0)
		return GL_CLAMP_TO_EDGE;

	logWarn("Invalid Wrap Mode: " << str);
	return GL_REPEAT;
}