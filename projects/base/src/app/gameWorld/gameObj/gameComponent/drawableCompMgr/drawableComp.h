#ifndef DRAWABLE_COMP_H
#define DRAWABLE_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

#include "app\gameWorld\gameObj\gameComponent\drawableCompMgr\drawableComp\textureProperties.h"
#include "app\gameWorld\gameObj\gameComponent\drawableCompMgr\drawableComp\renderPass.h"

class DrawableComp : public ABGameObjComp
{
public:
	DrawableComp();
	virtual ~DrawableComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

private:
	PROP_G_S(MeshData*, mMesh, MeshData);

	PROP_G_S(TextureProps, mDiffuseTexture, DiffuseTexture);
	PROP_G_S(TextureProps, mNormalTexture, NormalTexture);
	PROP_G_S(TextureProps, mSpecularTexture, SpecularTexture);
	PROP_G_S(TextureProps, mEmissiveTexture, EmissiveTexture);

	PROP_G_S(GLenum, mWinding, Winding);

	std::vector<RenderPass> mRenderPassList;

	PROP_G(bool, mIsTranparent, IsTranparent);
	PROP_G_S(float, mDistToCam, DistToCam);
	bool mDisableZBuffer;
	bool mDisableRotation;
};

#endif // DRAWABLE_COMP_H