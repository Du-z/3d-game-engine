#ifndef TEXTURE_PROPERTIES_H
#define TEXTURE_PROPERTIES_H

// Include GLEW first
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtx\transform.hpp>

#include "app\utilities.h"
#include "app\resMgr.h"

class TextureProps
{
public:
	TextureProps();
	~TextureProps();

	void setup(const Json::Value& textureProps);

	void setMagFilter(const std::string& str){mMagFilter = strToFilterMode(str);}
	void setMinFilter(const std::string& str){mMinFilter = strToFilterMode(str);}

	void setWrapModeX(const std::string& str){mWrapModeX = strToWrapMode(str);}
	void setWrapModeY(const std::string& str){mWrapModeY = strToWrapMode(str);}

private:
	PROP_G_S(GLuint, mTextureId, TextureId);

	PROP_G_S(GLint, mMagFilter, MagFilter);
	PROP_G_S(GLint, mMinFilter, MinFilter);

	PROP_G_S(GLint, mWrapModeX, WrapModeX);
	PROP_G_S(GLint, mWrapModeY, WrapModeY);

	GLint strToFilterMode(const std::string& str);
	GLint strToWrapMode(const std::string& str);
};

#endif // TEXTURE_PROPERTIES_H