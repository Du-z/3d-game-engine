#ifndef UI_TEXT_COMPONENT_MGR_H
#define UI_TEXT_COMPONENT_MGR_H

#include "uiTextCompMgr/uiTextComp.h"

class UiTextCompMgr
{
public:
	~UiTextCompMgr();
	static UiTextCompMgr* GetInstance();

	UiTextComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	UiTextComp* add(UiTextComp* component);

	UiTextComp* getComponent(const std::string& str);
	UiTextComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	UiTextCompMgr();
	static UiTextCompMgr* mInstance;

	std::vector<UiTextComp*> mCompList;
};

#endif // UI_TEXT_COMPONENT_MGR_H