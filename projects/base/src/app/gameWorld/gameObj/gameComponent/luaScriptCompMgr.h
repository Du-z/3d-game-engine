#ifndef LUA_SCRIPT_COMPONENT_MGR_H
#define LUA_SCRIPT_COMPONENT_MGR_H

#include "luaScriptCompMgr/luaScriptComp.h"

class LuaScriptCompMgr
{
public:
	~LuaScriptCompMgr();
	static LuaScriptCompMgr* GetInstance();

	LuaScriptComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	LuaScriptComp* add(LuaScriptComp* component);

	LuaScriptComp* getComponent(const std::string& str);
	LuaScriptComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	LuaScriptCompMgr();
	static LuaScriptCompMgr* mInstance;

	std::vector<LuaScriptComp*> mCompList;
};

#endif // LUA_SCRIPT_COMPONENT_MGR_H