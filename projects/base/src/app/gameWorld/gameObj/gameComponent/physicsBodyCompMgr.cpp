#include "PhysicsBodyCompMgr.h"

PhysicsBodyCompMgr* PhysicsBodyCompMgr::mInstance = NULL;

PhysicsBodyCompMgr* PhysicsBodyCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new PhysicsBodyCompMgr();

	return mInstance;
}

PhysicsBodyCompMgr::PhysicsBodyCompMgr()
{
}

PhysicsBodyCompMgr::~PhysicsBodyCompMgr()
{
	mInstance = NULL;
}

PhysicsBodyComp* PhysicsBodyCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	PhysicsBodyComp* comp = new PhysicsBodyComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

PhysicsBodyComp* PhysicsBodyCompMgr::add(PhysicsBodyComp* component)
{
	mCompList.push_back(component);
	return component;
}

PhysicsBodyComp* PhysicsBodyCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

PhysicsBodyComp* PhysicsBodyCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void PhysicsBodyCompMgr::tick(const float deltaT)
{
	profile(); 
	
	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}
}