#ifndef PARTICLE_EMITER_COMP_H
#define PARTICLE_EMITER_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

#include "app\gameWorld\gameObj\gameComponent\drawableCompMgr.h"

struct Particle
{
	glm::vec3 pos;
	glm::vec3 vel;
};

class ParticleEmitterComp : public ABGameObjComp
{
public:
	ParticleEmitterComp();
	virtual ~ParticleEmitterComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	void start();

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

private:
	DrawableComp* mDrawable;

	MeshData* mMeshReference;

	bool mIsActive;
	size_t mParticlesStarted;
	size_t mParticlesKilled;
	size_t mNumToSpawn; // how many particles to spawn per frame
	size_t mParticleReset; // used when a particle never dies
	float mSpawnCountdown; // countdowns until next particle spawn
	float mDeathCountdown; // countdowns until the particles start to die

	// from config file
	float mSpawnRate;
	float mLife; // how long the particle should live for
	glm::vec2 mSprayAngle; // in radians
	glm::vec2 mSprayAngleCenter; // in radians
	float mSpeedMin, mSpeedMax; // in m/s

	std::vector<Particle> mParticleList;

	unsigned int mVertsPerParticle;
	unsigned int mIndPerParticle;
	MeshData* generateMesh(const unsigned int particleCount);
};

#endif // PARTICLE_EMITER_COMP_H