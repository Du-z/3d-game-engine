#include "particleEmitterComp.h"


ParticleEmitterComp::ParticleEmitterComp()
{
	mType = Type::LUA_SCRIPT;
	mParentGameObj = NULL;
}

ParticleEmitterComp::~ParticleEmitterComp()
{

}

void ParticleEmitterComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	// set translation
	mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	mSpawnRate = (float)component["spawnRate"].asDouble();
	mLife = (float)component["life"].asDouble();
	mSprayAngle = glm::vec2(Utilities::degToRad((float)component["sprayAngle"]["x"].asDouble()), Utilities::degToRad((float)component["sprayAngle"]["y"].asDouble()));
	mSprayAngleCenter = glm::vec2(Utilities::degToRad((float)component["sprayAngleCenter"]["x"].asDouble()), Utilities::degToRad((float)component["sprayAngleCenter"]["y"].asDouble()));
	mSpeedMin = (float)component["speed"]["min"].asDouble();
	mSpeedMax = (float)component["speed"]["max"].asDouble();
	mParticleList.resize(component["count"].asInt());

	mMeshReference = ResMgr::GetInstance()->getMesh(component["mesh"].asString());

	MeshData* generatedMesh = generateMesh(mParticleList.size());
	ResMgr::GetInstance()->addMesh(strListToStr(nameIdList), generatedMesh);

	mDrawable = new	DrawableComp();
	DrawableCompMgr::GetInstance()->add(mDrawable);

	mDrawable->setup(nameIdList, component["particleDrawable"], mParentGameObj, this);
	mDrawable->setMeshData(generatedMesh);

	start();
}

MeshData* ParticleEmitterComp::generateMesh(const unsigned int particleCount)
{
	
	mVertsPerParticle = mMeshReference->getVertices().size();
	mIndPerParticle = mMeshReference->getIndices().size(); 

	std::vector<unsigned short> indices(mIndPerParticle * particleCount);
	std::vector<glm::vec3> vertices(mVertsPerParticle * particleCount);
	std::vector<glm::vec2> uvs(mVertsPerParticle * particleCount);
	std::vector<glm::vec3> normals(mVertsPerParticle * particleCount); 

	for(unsigned int i = 0; i < particleCount; i++)
	{
		for(unsigned int j = 0; j < mIndPerParticle; j++)
		{
			unsigned int index = (i * mIndPerParticle) + j;
			indices[index] = (i * mVertsPerParticle) + mMeshReference->getIndices()[j];
		}
		for(unsigned int j = 0; j < mVertsPerParticle; j++)
		{
			unsigned int index = (i * mVertsPerParticle) + j;
			vertices[index] = mMeshReference->getVertices()[j];
			uvs[index] = mMeshReference->getUvs()[j];
			normals[index] = mMeshReference->getNormals()[j];
		}
	}

	MeshData* meshData = new MeshData;
	meshData->create(indices, vertices, uvs, normals, GL_DYNAMIC_DRAW);

	return meshData;
}

void ParticleEmitterComp::eventReceiver(ABEvent* theEvent)
{

}

void ParticleEmitterComp::start()
{
	mIsActive = true;
	mParticlesStarted = 0;
	mParticlesKilled = 0;
	mNumToSpawn = 0;
	mParticleReset = -1;
	mSpawnCountdown = 0;
	mDeathCountdown = mLife;

	for(size_t i = 0; i < mParticleList.size(); ++i)
	{
		float randSpd = Utilities::randFloat(mSpeedMin, mSpeedMax);
		float randSpdDevisor = Utilities::randFloat(0.f, 1.f);
		float randSpdX = randSpd / randSpdDevisor;
		float randSpdY = randSpd / (1 - randSpdDevisor);

		float randDirX = Utilities::randFloat(-mSprayAngle.x / 2 - mSprayAngleCenter.x, mSprayAngle.x / 2 - mSprayAngleCenter.x);
		float randDirY = Utilities::randFloat(-mSprayAngle.y / 2 - mSprayAngleCenter.y, mSprayAngle.y / 2 - mSprayAngleCenter.y);

		int particleI = (mParticleReset - mNumToSpawn + i) % mParticleList.size();

		// Direction : Spherical coordinates to Cartesian coordinates conversion
		mParticleList[particleI].pos = glm::vec3(
			cos(randDirY) * sin(randDirX), 
			sin(randDirY),
			cos(randDirY) * cos(randDirX)
			);

		mParticleList[particleI].vel = glm::vec3(
			cos(randSpdY) * sin(randSpdX), 
			sin(randSpdY),
			cos(randSpdY) * cos(randSpdX)
			);
	}

}

void ParticleEmitterComp::tick(const float deltaT)
{
	profile();

	if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;

	glm::vec3 position = glm::vec3(mDerivedMatrix[3]);

	// check to see if more particles should be spawned
	if(mIsActive)
	{
		// if a particle is needed next frame spawn it now
		if(mSpawnCountdown <= 0) 
		{
			// add it to the current particlesStarted count, but make sure it doesn't overflow
			mNumToSpawn = (int)(deltaT / mSpawnRate);

			if(mParticlesStarted < mParticleList.size())
			{
				mParticlesStarted += mNumToSpawn;

				if(mParticlesStarted > mParticleList.size())
					mParticlesStarted = mParticleList.size();
			}
			else
			{
				mParticleReset = mParticleReset + mNumToSpawn;
			}

			mSpawnCountdown = mSpawnRate;
		}
		else
		{
			mSpawnCountdown -= deltaT; // countdown the time till the next spawn
			mNumToSpawn = 0;
		}

		// if a particle has a life lets see if we can start killing them
		if(mLife > 0)
		{
			if(mDeathCountdown <= 0)
			{
				// figure out what particles need to die
				mParticlesKilled = (int)fabs(mDeathCountdown / mSpawnRate) + 1;

				// check that all the particles that need to be spawned are now dead
				if(mParticlesKilled >= mParticleList.size()) 
				{
					mIsActive = false;
					return;
				}
			}
			mDeathCountdown -= deltaT;
		}
		// however if life == 0 it is set to respawn as we run out of particles, if they never die, and get moved back to the origPos
		else
		{
			if(mNumToSpawn > 0)
			{
				for(size_t i = 0; i < mNumToSpawn; ++i)
				{
					float randSpd = Utilities::randFloat(mSpeedMin, mSpeedMax);
					float randSpdDevisor = Utilities::randFloat(0.f, 1.f);
					float randSpdX = randSpd * randSpdDevisor;
					float randSpdY = randSpd * (1 - randSpdDevisor);

					float randDirX = Utilities::randFloat(-mSprayAngle.x / 2 - mSprayAngleCenter.x, mSprayAngle.x / 2 - mSprayAngleCenter.x);
					float randDirY = Utilities::randFloat(-mSprayAngle.y / 2 - mSprayAngleCenter.y, mSprayAngle.y / 2 - mSprayAngleCenter.y);

					int particleI = (mParticleReset - mNumToSpawn + i) % mParticleList.size();

					// Direction : Spherical coordinates to Cartesian coordinates conversion
					mParticleList[particleI].pos = glm::vec3(
						cos(randDirY) * sin(randDirX), 
						sin(randDirY),
						cos(randDirY) * cos(randDirX)
						);

					mParticleList[particleI].vel = glm::vec3(
						cos(randSpdY) * sin(randSpdX), 
						sin(randSpdY),
						cos(randSpdY) * cos(randSpdX)
						);
				}
			}
		}

		std::vector<glm::vec3>& vertices = mDrawable->getMeshData()->getVertices();
		std::vector<glm::vec3>& verticesReference = mMeshReference->getVertices();
		for(size_t i = mParticlesKilled; i < mParticlesStarted; ++i) 
		{
			mParticleList[i].pos += mParticleList[i].vel * deltaT;

			for(unsigned int j = 0; j < mVertsPerParticle; j++)
			{
				unsigned int index = (i * mVertsPerParticle) + j;

				vertices[index] = verticesReference[j] + mParticleList[i].pos;
			}
		}
		mDrawable->getMeshData()->updateBuffer(vertices, mDrawable->getMeshData()->getVertexBufferId());
	}

}