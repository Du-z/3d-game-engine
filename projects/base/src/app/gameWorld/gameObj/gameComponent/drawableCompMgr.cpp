#include "drawableCompMgr.h"

#include "glm/gtx/norm.hpp"

#include "app.h"

DrawableCompMgr* DrawableCompMgr::mInstance = NULL;

DrawableCompMgr* DrawableCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new DrawableCompMgr();

	return mInstance;
}

DrawableCompMgr::DrawableCompMgr()
{
	App::GetInstance()->getDynamicsWorld()->setDebugDrawer(&mDebugDrawer);

	mDrawBulletDebug = false;
	mDrawWireFrame = false;
}

DrawableCompMgr::~DrawableCompMgr()
{
	mInstance = NULL;
}

DrawableComp* DrawableCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	DrawableComp* comp = new DrawableComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

DrawableComp* DrawableCompMgr::add(DrawableComp* component)
{
	mCompList.push_back(component);
	return component;
}

DrawableComp* DrawableCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

DrawableComp* DrawableCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void DrawableCompMgr::tick(const float deltaT)
{
	profile();
	
	GLFWwindow* window = App::GetInstance()->getWindow();
	
	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(mDrawWireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	// sort the drawable by transparency and distance
	sort();

	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}

	if(mDrawBulletDebug)
		drawBulletDebug();

	// Swap buffers
	glfwSwapBuffers(window);
}

void DrawableCompMgr::sort()
{
	profile();
	glm::vec3 camPos = CameraCompMgr::GetInstance()->getActiveCamera()->getDerivedTranslation();
	int size = mCompList.size();

	// update distance info
	for (int i = 0; i < size; i++)
	{
		mCompList[i]->setDistToCam(glm::length2(camPos - mCompList[i]->getDerivedTranslation()));
	}

	// shuffle the transparent stuff to the back
	int transCount = 0;
	for (int i = size - 1; i >= 0; i--)
	{
		int nextSwapSpot = (size - 1) - transCount;
		if(mCompList[i]->getIsTranparent()) // if it is transparent AND
		{
			if(nextSwapSpot != i) // Its not swapping with itself
				std::swap(mCompList[i], mCompList[nextSwapSpot]);// Then we can swap!
			transCount++;
		}		
	}

	// sort the non transparent stuff
	for (int i = 1, j; i < size - transCount; i++)
	{
		j = i;
		DrawableComp* tmp = mCompList[i];
		while (j > 0 && tmp->getDistToCam() < mCompList[j - 1]->getDistToCam())
		{
			mCompList[j] = mCompList[j - 1];
			j--;
		}
		mCompList[j] = tmp; 
	}

	// sort the transparent stuff
	for (int i = size - transCount, j; i < size; i++)
	{
		j = i;
		DrawableComp* tmp = mCompList[i];
		while (j > 0 && tmp->getDistToCam() > mCompList[j - 1]->getDistToCam() && j < size - transCount)
		{
			mCompList[j] = mCompList[j - 1];
			j--;
		}
		mCompList[j] = tmp; 
	}
}

void DrawableCompMgr::drawBulletDebug()
{
	profile();

	CameraComp* camera = CameraCompMgr::GetInstance()->getActiveCamera();

	mDebugDrawer.SetMatrices(camera->getViewMatrix(), camera->getProjectionMatrix());
	App::GetInstance()->getDynamicsWorld()->debugDrawWorld();
}