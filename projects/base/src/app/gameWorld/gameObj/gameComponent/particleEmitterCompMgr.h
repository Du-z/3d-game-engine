#ifndef PARTICLE_EMITER_COMPONENT_MGR_H
#define PARTICLE_EMITER_COMPONENT_MGR_H

#include "particleEmitterCompMgr/particleEmitterComp.h"

class ParticleEmitterCompMgr
{
public:
	~ParticleEmitterCompMgr();
	static ParticleEmitterCompMgr* GetInstance();

	ParticleEmitterComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	ParticleEmitterComp* add(ParticleEmitterComp* component);

	ParticleEmitterComp* getComponent(const std::string& str);
	ParticleEmitterComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	ParticleEmitterCompMgr();
	static ParticleEmitterCompMgr* mInstance;

	std::vector<ParticleEmitterComp*> mCompList;
};

#endif // PARTICLE_EMITER_COMPONENT_MGR_H