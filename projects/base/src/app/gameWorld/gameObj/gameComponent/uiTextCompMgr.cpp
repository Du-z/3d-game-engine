#include "uiTextCompMgr.h"

UiTextCompMgr* UiTextCompMgr::mInstance = NULL;

UiTextCompMgr* UiTextCompMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new UiTextCompMgr();

	return mInstance;
}

UiTextCompMgr::UiTextCompMgr()
{
}

UiTextCompMgr::~UiTextCompMgr()
{
	mInstance = NULL;
}

UiTextComp* UiTextCompMgr::add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	UiTextComp* comp = new UiTextComp();

	comp->setup(parentsNameIdList, component, parentGameObj, parentGameComp);
	add(comp);

	return comp;
}

UiTextComp* UiTextCompMgr::add(UiTextComp* component)
{
	mCompList.push_back(component);
	return component;
}

UiTextComp* UiTextCompMgr::getComponent(const std::string& str)
{
	unsigned int id = ABGameObjComp::calculateId(str);

	return getComponent(id);
}

UiTextComp* UiTextCompMgr::getComponent(const unsigned int id)
{
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		if(mCompList[i]->compareId(id))
		{
			return mCompList[i];
		}
	}

	return NULL;
}

void UiTextCompMgr::tick(const float deltaT)
{
	profile();
	
	// tick for any child game components
	for(size_t i = 0; i < mCompList.size(); ++i)
	{
		mCompList[i]->tick(deltaT);
	}
}