#ifndef PHYSICS_BODY_COMPONENT_MGR_H
#define PHYSICS_BODY_COMPONENT_MGR_H

#include "physicsBodyCompMgr/physicsBodyComp.h"

class PhysicsBodyCompMgr
{
public:
	~PhysicsBodyCompMgr();
	static PhysicsBodyCompMgr* GetInstance();

	PhysicsBodyComp* add(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);
	PhysicsBodyComp* add(PhysicsBodyComp* component);

	PhysicsBodyComp* getComponent(const std::string& str);
	PhysicsBodyComp* getComponent(const unsigned int id);

	void tick(const float deltaT);

private:
	PhysicsBodyCompMgr();
	static PhysicsBodyCompMgr* mInstance;

	std::vector<PhysicsBodyComp*> mCompList;
};

#endif // PHYSICS_BODY_COMPONENT_MGR_H