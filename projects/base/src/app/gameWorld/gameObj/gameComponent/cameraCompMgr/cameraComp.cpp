#include "cameraComp.h"

#include <glm/gtc/matrix_transform.hpp>

CameraComp::CameraComp()
{
	mType = Type::CAMERA;
	mCameraMode = Mode::UNSET;

	mParentGameObj = NULL;
}

CameraComp::~CameraComp()
{

}

void CameraComp::setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp)
{
	std::string nameId = component["name"].asString();
	std::vector<std::string> nameIdList = parentsNameIdList;
	nameIdList.push_back(nameId);
	setId(nameIdList);

	// set translation
	mMatrix[3] = glm::vec4(component["pos"]["x"].asDouble(), component["pos"]["y"].asDouble(), component["pos"]["z"].asDouble(), 1);
	if(parentGameComp != NULL)
		mDerivedMatrix = parentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = parentGameObj->getDerivedMatrix() * mMatrix;

	mParentGameObj = parentGameObj;
	mParentGameComp = parentGameComp;

	mCameraMode = strToGameComponentEnum(component["mode"].asString());

	float horizontalAngle = Utilities::degToRad(component["angle"]["hor"].asDouble());
	float verticalAngle = Utilities::degToRad(component["angle"]["vert"].asDouble());
	
	mRotation = glm::vec3(verticalAngle, horizontalAngle, 0);
	mFov = 45.f;

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.01 unit <-> 1000 units
	mProjectionMatrix = glm::perspective(mFov, 16.0f / 9.0f, 0.01f, 1000.0f);

	EventMgr::GetInstance()->addSubscriber(Event::CURSOR_MOVED, this);
}

void CameraComp::eventReceiver(ABEvent* theEvent)
{
	// Check the event type
	if(theEvent->getEventType() == Event::CURSOR_MOVED)
	{
		CursorMoved* cursor = static_cast<CursorMoved*>(theEvent);

		mRotation.x += -cursor->getDeltaYPos() * 0.005f;
	}
}

void CameraComp::tick(const float deltaT)
{
	profile();

	if(mParentGameComp != NULL)
		mDerivedMatrix = mParentGameComp->getDerivedMatrix() * mMatrix;
	else
		mDerivedMatrix = mParentGameObj->getDerivedMatrix() * mMatrix;

	glm::quat quaternion = glm::quat(mRotation);
	glm::mat4 rotMat = glm::mat4_cast(quaternion);

	mDerivedMatrix *= rotMat;

	mViewMatrix = glm::inverse(mDerivedMatrix);
}

CameraComp::Mode CameraComp::strToGameComponentEnum(const std::string& str)
{
	if(strcmp(str.c_str(), "free") == 0)
		return CameraComp::Mode::FREE;
	if(strcmp(str.c_str(), "firstPerson") == 0)
		return CameraComp::Mode::FIRST_PERSON;
	if(strcmp(str.c_str(), "thirdPerson") == 0)
		return CameraComp::Mode::THIRD_PERSON;

	return CameraComp::Mode::UNSET;
}