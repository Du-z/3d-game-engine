#ifndef CAMERA_COMP_H
#define CAMERA_COMP_H

#include "app\gameWorld\gameObj\abGameComponent.h"

class CameraComp : public ABGameObjComp
{
public:
	CameraComp();
	virtual ~CameraComp();

	virtual void setup(const std::vector<std::string>& parentsNameIdList, const Json::Value& component, GameObj* parentGameObj, ABGameObjComp* parentGameComp = NULL);

	virtual void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

	enum Mode
	{
		UNSET = -1,

		FREE,
		FIRST_PERSON,
		THIRD_PERSON,

		COUNT
	};

private:

	PROP_GR(glm::mat4, mViewMatrix, ViewMatrix);
	PROP_GR(glm::mat4, mProjectionMatrix, ProjectionMatrix);

	glm::vec2 mViewAngle;
	PROP_G(float, mFov, Fov);

	CameraComp::Mode strToGameComponentEnum(const std::string& str);
	CameraComp::Mode mCameraMode;

	glm::vec3 mRotation;
};

#endif // CAMERA_COMP_H