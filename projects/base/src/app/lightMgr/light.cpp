#include "light.h"

#include "glm\glm.hpp"
#include <glm/gtc/matrix_transform.hpp>

#include "app\logger.h"
#include "app\renderer\camera.h"
#include "app\gameObjMgr.h"

Light::Light()
{
	depthProgram = ResMgr::GetInstance()->getShader("rsc/shaders/depthRTT.vert", "rsc/shaders/depthRTT.frag");;
}

Light::~Light()
{
}

void Light::setup(const Json::Value& lightInfo)
{
	mPos = glm::vec3(lightInfo["pos"]["x"].asDouble(), lightInfo["pos"]["y"].asDouble(), lightInfo["pos"]["z"].asDouble());
	mType = strToLightType(lightInfo["type"].asString());
	mPower = lightInfo["power"].asDouble();

	mDepthTexture = setupDepthTexture();
}

GLuint Light::setupDepthTexture()
{
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	GLuint FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// Depth texture. Slower than a depth buffer, but you can sample it later in your shader
	GLuint depthTexture;
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT16, 1024, 1024, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	// No color output in the bound framebuffer, only depth.
	glDrawBuffer(GL_NONE);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return 0;

	return FramebufferName;
}

Light::Type Light::strToLightType(const std::string& str)
{
	if(strcmp(str.c_str(), "point") == 0)
		return Light::POINT;
	if(strcmp(str.c_str(), "spot") == 0)
		return Light::SPOT;
	if(strcmp(str.c_str(), "dir") == 0)
		return Light::DIRECTIONAL;

	logWarn("Invalid Light Type: " << str);
	return Light::POINT;
}

void Light::computeDepthMap()
{
	// Render to our framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, mDepthTexture);
	glViewport(0,0,1024,1024); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(depthProgram->ProgramId);

	// Compute the MVP matrix from the light's point of view
	glm::mat4 depthProjectionMatrix;
	glm::mat4 depthViewMatrix;
	//if(mType == POINT)// for point light
	//{
	depthProjectionMatrix = glm::ortho<float>(-10,10,-10,10,-10,20);
	depthViewMatrix = glm::lookAt(glm::vec3(0), glm::vec3(0,-1,0), glm::vec3(0,1,0));
	//}

	glm::mat4 depthModelMatrix = glm::mat4(1.0);
	glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(depthProgram->MvpId, 1, GL_FALSE, &depthMVP[0][0]);

	std::vector<GameObj*>* goList = GameObjMgr::GetInstance()->getGOList();

	for(size_t i = 0, size = goList->size(); i < size; ++i)
	{
		gameObjList[i]->draw();
	}
}