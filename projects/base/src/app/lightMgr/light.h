#ifndef LIGHT_H
#define LIGHT_H

#include <glm\glm.hpp>

namespace Light
{
	enum Type
	{
		UNSET = -1,
		POINT,
		SPOT,
		DIRECTIONAL,
		COUNT
	};
}

struct LightData
{
	Light::Type type;
	glm::vec3 pos;
	float power;
};

#endif // LIGHT_H