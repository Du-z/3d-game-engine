#ifndef EVENT_MGR_H
#define EVENT_MGR_H

#include <vector>

#include "app/eventMgr/event.h"

class EventMgr
{
public:
	~EventMgr();
	static EventMgr* GetInstance();

	void notifySubscribers();
	void addSubscriber(Event::Types eventType, ABEventReciver* eventReceiver);
	void clearSubscribers();

	void addEvent(ABEvent* theEvent);

private:
	EventMgr();
	static EventMgr* mInstance;

	std::vector<std::vector<ABEventReciver*>> subscriberList;
	std::vector<ABEvent*> eventList;
};

#endif // EVENT_MGR_H