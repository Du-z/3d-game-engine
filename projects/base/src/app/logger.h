#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <sstream>
#include <iomanip> // might be used for std::setprecision

#include "utilities.h"

#define ERROR 1
#define WARN 2
#define INFO 3

#if defined(DEBUG) || defined(_DEBUG)
	#define CUR_LOG_LEVEL INFO
#else
	#define CUR_LOG_LEVEL WARN
#endif

#if CUR_LOG_LEVEL >= ERROR
#define logError(LOG_TEXT) logFunc(LOG_TEXT, "ERROR")
#else
#define logError(LOG_TEXT)
#endif

#if CUR_LOG_LEVEL >= WARN
#define logWarn(LOG_TEXT) logFunc(LOG_TEXT, "WARN")
#else
#define logWarn(LOG_TEXT)
#endif

#if CUR_LOG_LEVEL >= INFO
#define logInfo(LOG_TEXT) logFunc(LOG_TEXT, "INFO")
#else
#define logInfo(LOG_TEXT)
#endif

#define logFunc(LOG_TEXT, LOG_LEVEL)																								\
{																																	\
 std::stringstream ss;																												\
 ss << LOG_LEVEL << ": " << Utilities::getFileNameFromDir(__FILE__) << '(' << __LINE__ << ')' << " - " << LOG_TEXT << std::endl;	\
 Logger::GetInstance()->writeLog(ss.str());																							\
}

class Logger
{
public:
	~Logger();
	static Logger* GetInstance();

	void setDir(const std::string& dir);

	// should not be used directly, use the macros instead
	void writeLog(const std::string& str);

private:
	Logger();
	static Logger* mInstance;

	void clearLog();

	std::string mDir;
};

#endif // LOGGER_H