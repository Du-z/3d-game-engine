#ifndef CONTROL_MGR_H
#define CONTROL_MGR_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class InputMgr
{
public:
	~InputMgr();
	static InputMgr* GetInstance();

	void setup(GLFWwindow* window);

private:
	InputMgr();
	static InputMgr* mInstance;

	static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

	static void cursorCallback(GLFWwindow* window, double xPos, double yPos);
	static double mLastXPos;
	static double mLastYPos;
};

#endif // CONTROL_MGR_H