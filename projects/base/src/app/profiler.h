#ifndef PROFILER_H
#define PROFILER_H

#include "app\profiler\profileStack.h"

//#include "app\gameWorld\gameObj\gameComponent\drawable.h"

#include "app\eventMgr.h"

class Profiler : public ABEventReciver
{
public:
	~Profiler();
	static Profiler* GetInstance();

	void start(const std::string& name);
	void stop(const std::string& name);

	void endFrame();

	virtual void eventReceiver(ABEvent* theEvent);

private:
	Profiler();
	static Profiler* mInstance;

	//Drawable mDrawable;

	// when set to true, toFile() is called file during the next endFrame().
	bool mToFile;
	void toFile();

	PROP_GR((std::map<std::string, ProfileStack>), mProfileStack, ProfileStack);
	PROP_G(std::string, mNextLvl, mNextLvl);

	PROP_G(int, mSecondIndex, SecondIndex);
	PROP_G(int, mSecondIndexCount, SecondIndexCount);
	int mCurrentSecond;

	PROP_G(int, mChartDepth, ChartDepth);
};

#endif // PROFILER_H