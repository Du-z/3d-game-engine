#include "eventMgr.h"

#include "app/profiler/profile.h"

EventMgr* EventMgr::mInstance = NULL;
EventMgr* EventMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new EventMgr();

	return mInstance;
}

EventMgr::EventMgr()
{
	subscriberList.resize(Event::COUNT);
}

EventMgr::~EventMgr()
{
	mInstance = NULL;
}

void EventMgr::notifySubscribers()
{
	profile();
	
	// go through each new event
	for(size_t i = 0; i < eventList.size(); ++i)
	{
		ABEvent* theEvent = eventList[i];
		Event::Types eventType = theEvent->getEventType();
		// go through all the actors subscribed to that event
		for(size_t j = 0, size = subscriberList[eventType].size(); j < size; ++j)
		{
			subscriberList[eventType][j]->eventReceiver(theEvent);
		}
		delete theEvent;
	}
	eventList.clear();
}

void EventMgr::addSubscriber(Event::Types eventType, ABEventReciver* eventReceiver)
{
	subscriberList[eventType].push_back(eventReceiver);
}

void EventMgr::clearSubscribers()
{
	for(size_t i1 = 0; i1 < Event::COUNT; ++i1)
	{
		subscriberList[i1].clear();
	}
}

void EventMgr::addEvent(ABEvent* theEvent)
{
	eventList.push_back(theEvent);
}