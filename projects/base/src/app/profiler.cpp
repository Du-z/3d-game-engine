#include "profiler.h"

#include "app\profiler\profile.h"
#include "utilities.h"

#include "GLFW/glfw3.h"

Profiler* Profiler::mInstance = NULL;

Profiler* Profiler::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new Profiler();

	return mInstance;
}

Profiler::Profiler()
{
	mSecondIndexCount = 60;
	mSecondIndex = 0;


	mChartDepth = 0;

	mNextLvl = "";

	mToFile = false;

	EventMgr::GetInstance()->addSubscriber(Event::KEY_RELEASED, this);
}

Profiler::~Profiler()
{
	mInstance = NULL;
}

void Profiler::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::KEY_RELEASED)
	{
		KeyReleased* key = static_cast<KeyReleased*>(theEvent);
		switch (key->getKey())
		{
		case GLFW_KEY_F12:
			mToFile = true;
			break;
		case GLFW_KEY_F11:
			mChartDepth++;
			break;
		case GLFW_KEY_F10:
			mChartDepth--;
			if(mChartDepth < 0)
				mChartDepth = 0;
			break;
		}
	}
}

void Profiler::start(const std::string& name)
{
	if(mNextLvl.size() == 0) // this is the highest level of the current stack
	{
		mProfileStack[name].start(name); // so we should start the next

		mNextLvl = name;
	}
	else
	{
		mProfileStack[mNextLvl].upStackStart(name);
	}
}

void Profiler::stop(const std::string& name)
{
	if(mNextLvl.size() == 0) // this is the highest level of the current stack
	{
		logWarn("Profiler tried to stop the root.");
	}
	else
	{
		bool result = mProfileStack[mNextLvl].stop(name);

		if(result) // we just stopped the highest level
		{
			mNextLvl = "";
		}
	}
}

void Profiler::endFrame()
{
	if(mToFile)
	{
		toFile();
		mToFile = false;
	}

	//buildChart();

	int time = (int)(glfwGetTime());
	if(mCurrentSecond != time)
	{
		mCurrentSecond = time;


		mSecondIndex++;
		if(mSecondIndex >= mSecondIndexCount - 1)
			mSecondIndex = 0;

		for (auto it = mProfileStack.begin(); it != mProfileStack.end(); ++it)
		{
			it->second.startSecond();
		}
	}
}

void Profiler::toFile()
{
	std::string dir = "profiler " + Utilities::currentDateTimeDirSafe() + ".txt";

	std::ofstream file(dir, std::ios::trunc);
	if (file.is_open())
	{
		file.precision(3);
		file << "Results from " << Utilities::currentDateTime() << ".\nTimes run per second, averaged over the last " << mSecondIndexCount << " seconds.\n\n";

		for (auto it = mProfileStack.begin(); it != mProfileStack.end(); ++it)
		{
			it->second.toFile(file, 0);
		}

		file.close();

		logInfo("Printed profiler data to '" << dir << "'.");
	}
	else
	{
		logWarn("Could not print profiler data to '" << dir << "'.");
	}
}