#ifndef TEXT_HPP
#define TEXT_HPP

#include "app/resMgr.h"
#include "app/utilities.h"
#include "app\gameWorld\gameObj\gameComponent\drawable.h"

class Text
{
public:
	Text();
	~Text();

	void load(const std::string& texturePath, const std::string& str, const float size, bool isOsd);
	void draw();

	MeshData* buildMesh(const float size);
	void setString(const std::string& str);

	void setPos(const float x, const float y){mDrawable.translate(x * 2, y * 2, 0);}
	void setPos(const float x, const float y, const float z){mDrawable.translate(x, y, z);}

	void setSize(const float s);

private:
	DrawableComp mDrawable;

	MeshData* mesh;

	std::string mStr;
	float mSize;
};

#endif // TEXT_HPP