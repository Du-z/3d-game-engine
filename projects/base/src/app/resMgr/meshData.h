#ifndef MESH_DATA_H
#define MESH_DATA_H

#include <vector>
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "app/utilities.h"

class MeshData
{
public:
	MeshData();
	~MeshData();

	void generate(int x, int z, float quadSize, int randYMax = 0, int smoothPasses = 0);

	//Generates a vector
	void generateVerts(int x, int z, float quadSize, int randYMax = 0, int smoothPasses = 0); // default (= 0) smoothPasses will create a smooth terrain
	void generateNorm();
	void generateUv(int x, int z, float maxU = 0, float maxV = 0); // if the values are left at default (= 0), the texture will wrap
	void generateInd(int x, int z);

	// loads a compatible assimp file onto GPU
	void load(const std::string& path);

	// loads vectors onto GPU
	void load(GLenum usage = GL_STATIC_DRAW);

	void create(
		const std::vector<unsigned short>& indices,
		const std::vector<glm::vec3>& vertices,
		const std::vector<glm::vec2>& uvs,
		const std::vector<glm::vec3>& normals,
		GLenum usage = GL_STATIC_DRAW
		);

	void updateBuffer(const std::vector<unsigned short>& buff, GLuint bufferId);
	void updateBuffer(const std::vector<glm::vec2>& buff, GLuint bufferId);
	void updateBuffer(const std::vector<glm::vec3>& buff, GLuint bufferId);

	unsigned short getIndicesCount(){return mIndices.size();}

private:
	PROP_G(GLuint, mVertexArrayId, VertexArrayId);

	PROP_G(GLuint, mVertexBufferId, VertexBufferId);
	PROP_G(GLuint, mUvBufferId, UvBufferId);
	PROP_G(GLuint, mNormalBufferId, NormalBufferId);
	PROP_G(GLuint, mIndexBufferId, IndexBufferId);

	PROP_GR(std::vector<unsigned short>, mIndices, Indices);
	PROP_GR(std::vector<glm::vec3>, mVertices, Vertices);
	PROP_GR(std::vector<glm::vec2>, mUvs, Uvs);
	PROP_GR(std::vector<glm::vec3>, mNormals, Normals);

	// loads a compatible assimp file into vectors
	bool loadFile(const std::string& path);

	// creates a default buffer
	void createBuffers(GLenum usage = GL_STATIC_DRAW);

	// could be templated
	GLuint createBuffer(const std::vector<unsigned short>& buff, GLenum usage = GL_STATIC_DRAW);
	GLuint createBuffer(const std::vector<glm::vec2>& buff, GLuint index, GLenum usage = GL_STATIC_DRAW);
	GLuint createBuffer(const std::vector<glm::vec3>& buff, GLuint index, GLenum usage = GL_STATIC_DRAW);

	void smoothY(std::vector<float>& randY, int x, int z);

	PROP_G(int, mMeshX, MeshX);
	PROP_G(int, mMeshZ, MeshZ);
};

#endif // MESH_DATA_H