#ifndef SHADER_DATA_H
#define SHADER_DATA_H

#include <GL/glew.h>

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);

#endif // SHADER_DATA_H
