#include "text.h"

#include <vector>
#include <cstring>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "app\profiler\profile.h"

Text::Text()
{

}

Text::~Text()
{

}

void Text::load(const std::string& texturePath, const std::string& str, const float size, bool isOsd)
{	
	mDrawable.setDiffuseTexture(ResMgr::GetInstance()->getTexture(texturePath, true));

	if(isOsd)
		mDrawable.setShaderData(ResMgr::GetInstance()->getShader("rsc/shaders/font.vert", "rsc/shaders/simple.frag"));
	else
		mDrawable.setShaderData(ResMgr::GetInstance()->getShader("rsc/shaders/billboard.vert", "rsc/shaders/billboard.frag"));

	mSize = size;

	setString(str); // this will build the mesh too

	mDrawable.setMagFilter(GL_LINEAR);
	mDrawable.setMinFilter(GL_LINEAR_MIPMAP_LINEAR);

	mDrawable.setBlendModeSrc(GL_SRC_ALPHA);
	mDrawable.setBlendModeDst(GL_ONE_MINUS_SRC_ALPHA);
}

void Text::setString(const std::string& str)
{
	if(strcmp(mStr.c_str(), str.c_str()) == 0) // we don't need to do all this hard math if the string is the same
		return;
	
	mStr = str;
	
	mDrawable.setMesh(buildMesh(mSize));
}

void Text::setSize(const float s)
{
	if(mSize == s) // we don't need to do all this hard math if the size is the same
		return;

	mSize = s;
	
	mDrawable.setMesh(buildMesh(s));
}

MeshData* Text::buildMesh(const float size)
{
	std::vector<unsigned short> indices;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	
	unsigned int length = mStr.size();
	float offsetY = 0;
	int offsetX = 0;

	// Fill buffers
	for ( unsigned int i = 0, indicies = 0; i < length; i++ ){

		if(mStr[i] == '\n')
		{
			offsetY -= size;
			offsetX = i + 1;
		}
		else
		{
			// Build Verts
			glm::vec3 vertex_up_left((i - offsetX) * size, offsetY + size, 0);
			glm::vec3 vertex_up_right((i - offsetX) * size + size, offsetY + size, 0);
			glm::vec3 vertex_down_right((i - offsetX) * size + size, offsetY, 0);
			glm::vec3 vertex_down_left((i - offsetX) * size, offsetY, 0);

			vertices.push_back(vertex_up_left);
			vertices.push_back(vertex_down_left);
			vertices.push_back(vertex_up_right);

			vertices.push_back(vertex_down_right);
			vertices.push_back(vertex_up_right);
			vertices.push_back(vertex_down_left);

			// Build UV
			char character = mStr[i];
			float uvX = (character%16)/16.0f;
			float uvY = (character/16)/16.0f;

			glm::vec2 uv_up_left(uvX, 1.0f - uvY);
			glm::vec2 uv_up_right(uvX+1.0f/16.0f, 1.0f - uvY);
			glm::vec2 uv_down_right(uvX+1.0f/16.0f, 1.0f - (uvY + 1.0f/16.0f));
			glm::vec2 uv_down_left(uvX, 1.0f - (uvY + 1.0f/16.0f));

			uvs.push_back(uv_up_left);
			uvs.push_back(uv_down_left);
			uvs.push_back(uv_up_right);

			uvs.push_back(uv_down_right);
			uvs.push_back(uv_up_right);
			uvs.push_back(uv_down_left);

			// Build Normals
			glm::vec3 norm(0, 1, 0);

			normals.push_back(norm);
			normals.push_back(norm);
			normals.push_back(norm);

			normals.push_back(norm);
			normals.push_back(norm);
			normals.push_back(norm);

			// Build Indicies
			indices.push_back(indicies++);
			indices.push_back(indicies++);
			indices.push_back(indicies++);

			indices.push_back(indicies++);
			indices.push_back(indicies++);
			indices.push_back(indicies++);
		}
	}

	mesh = new MeshData();
	mesh->load(indices, vertices, uvs, normals);

	return mesh;
}

void Text::draw()
{
	profile();
	
	ShaderData* shaderData;

	///////// INITIAL PASS \\\\\\\\\

	shaderData = mDrawable.getShaderData();
	glUseProgram(shaderData->ProgramId);

	glBlendFunc(mDrawable.getBlendModeSrc(), mDrawable.getBlendModeDst());

	mDrawable.setupAmbientUniforms(shaderData);
	mDrawable.draw();
}

//void cleanupText2D(){
//
//	// Delete buffers
//	glDeleteBuffers(1, &Text2DVertexBufferID);
//	glDeleteBuffers(1, &Text2DUVBufferID);
//
//	// Delete texture
//	glDeleteTextures(1, &Text2DTextureID);
//
//	// Delete shader
//	glDeleteProgram(Text2DShaderID);
//}