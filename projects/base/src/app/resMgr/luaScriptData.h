#ifndef LUA_SCRIPT_DATA_H
#define LUA_SCRIPT_DATA_H

#include <string>

#include "app/utilities.h"

class LuaScriptData
{
public:
	LuaScriptData();
	~LuaScriptData();

	bool load(const std::string& dir);

private:
	PROP_GR(std::string, mScript, Script);
};

#endif // LUA_SCRIPT_DATA_H