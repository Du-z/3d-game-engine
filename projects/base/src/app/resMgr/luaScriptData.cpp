#include "luaScriptData.h"

#include <iostream>
#include <fstream>

#include "app\logger.h"

LuaScriptData::LuaScriptData()
{

}

LuaScriptData::~LuaScriptData()
{

}

bool LuaScriptData::load(const std::string& dir)
{
	// Read the script code from the file
	std::ifstream stream(dir, std::ios::in);
	if(stream.is_open()){
		std::string line = "";
		while(getline(stream, line))
			mScript += "\n" + line;
		stream.close();
	}else{		
		logError("Cannot open shader '" << dir << "'");
		return false;
	}

	return true;
}