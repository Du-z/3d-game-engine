#include "meshData.h"

#include "app\logger.h"

// Include AssImp
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

MeshData::MeshData()
{
	mVertexArrayId = 0;

	mVertexBufferId = 0;
	mUvBufferId = 0;
	mNormalBufferId = 0;
	mIndexBufferId = 0;

	mMeshX = 0;
	mMeshZ = 0;
}

MeshData::~MeshData()
{

}

void MeshData::generate(int x, int z, float quadSize, int randYMax, int smoothPasses)
{	
	generateVerts(x, z, quadSize, randYMax, smoothPasses);
	generateInd(x, z);
	generateNorm();
	generateUv(x, z);
	
	// add the mesh to the GPU
	load();
}

void MeshData::generateVerts(int x, int z, float quadSize, int randYMax, int smoothPasses)
{

	mMeshX = x;
	mMeshZ = z;
	
	int totalVerts = (x + 1) * (z + 1);

	// rect size
	float xSize = quadSize;
	float zSize = quadSize;

	// Start pos
	float startPosX = ((float)x / 2) * -xSize;
	float startPosZ = ((float)z / 2) * -zSize;
	
	mVertices.resize(totalVerts);
	
	// get some smoothed random Zs
	std::vector<float> randY;
	randY.resize(totalVerts); 

	// create a list of random floats
	for(int i = 0; i < randY.size(); ++i)
	{
		randY[i] = Utilities::randFloat(0, randYMax);
	}

	for(int i = 0; i < smoothPasses; ++i)
	{
		smoothY(randY, x, z); 
	}

	// create the mesh
	for(int xCount = 0, zCount = 0, i = 0; zCount < z + 1; ++zCount, xCount = 0)
	{
		for(; xCount < x + 1; ++xCount)
		{
			mVertices[i] = glm::vec3(startPosX + xCount * xSize, randY[i], startPosZ + zCount * zSize);
			++i;
		}
	}
}

void MeshData::generateNorm()
{	
	mNormals.resize(mVertices.size());

	for( int i = 0; i < mIndices.size(); i += 1 )
	{
		mNormals[mIndices[i]] = glm::vec3(0,0,0);
	}
	// create the normals
	for( int i = 0; i < mIndices.size(); i += 3 )
	{
		// get the three vertices that make the faces
		glm::vec3 p1 = mVertices[mIndices[i+0]];
		glm::vec3 p2 = mVertices[mIndices[i+1]];
		glm::vec3 p3 = mVertices[mIndices[i+2]];

		glm::vec3 v1 = glm::normalize(p2 - p1);
		glm::vec3 v2 = glm::normalize(p3 - p1);
		glm::vec3 normal = glm::cross(v1, v2);

		normal = glm::normalize(normal);

		// Store the face's normal for each of the vertices that make up the face.
		mNormals[mIndices[i+0]] += (normal);
		mNormals[mIndices[i+1]] += (normal);
		mNormals[mIndices[i+2]] += (normal);
	}
	for( int i = 0; i < mIndices.size(); i += 1 )
	{
		mNormals[mIndices[i]] = glm::normalize(mNormals[mIndices[i]]);
	}
}

void MeshData::generateUv(int x, int z, float maxU, float maxV)
{
	int totalVerts = (x + 1) * (z + 1);

	mUvs.resize(totalVerts);
	
	// create the mesh
	for(int xCount = 0, zCount = 0, i = 0; zCount < z + 1; ++zCount, xCount = 0)
	{
		for(; xCount < x + 1; ++xCount)
		{
			float u;
			if(maxU == 0)
				u = xCount * 1;
			else
				u = ((float)xCount / x) * maxU;

			float v;
			if(maxV == 0)
				v = zCount * 1;
			else
				v = ((float)zCount / z) * maxV + (1 - maxV);

			mUvs[i] = glm::vec2(u, v);
			++i;
		}
	}
}

void MeshData::generateInd(int x, int z)
{
	int totalQuads = x * z;
	int totalInd = totalQuads * 6;

	mIndices.resize(totalInd);
	
	// index the mesh
	for(int quadCount = 0, i = 0, col = 0, row = 0, swapper = 0; quadCount < totalQuads; ++quadCount)
	{
		int a = row * (x + 1) + col + 1;
		int b = ((x + 1) * (row + 1)) + col + 1;
		int c = ((x + 1) * (row + 1)) + col;
		int d = row * (x + 1) + col;

		col++;
		if(col >= x)
		{
			col = 0;
			row++;
			if(x % 2 == 0)
				swapper++;
		}

		if(swapper == 0)
		{
			mIndices[i++] = d;
			mIndices[i++] = c;
			mIndices[i++] = a;
			mIndices[i++] = c;
			mIndices[i++] = b;
			mIndices[i++] = a;
		}
		else
		{
			mIndices[i++] = d;
			mIndices[i++] = b;
			mIndices[i++] = a;
			mIndices[i++] = d;
			mIndices[i++] = c;
			mIndices[i++] = b;
		}		
		swapper = (swapper + 1) % 2;
	}
}

void MeshData::smoothY(std::vector<float>& randY, int x, int z)
{
	// smooth the random floats

	// need to use a buffer otherwise the previously smoothed will effect the result
	std::vector<float> randYBuffer;
	randYBuffer.resize(randY.size());

	for(int col = 0, row = 0, curTarget = 0; curTarget < randY.size() - 1; )
	{

		curTarget = col + row * (x+1);
		int left = curTarget - 1;
		int right = curTarget + 1;
		int below = curTarget - (x+1);
		int above = curTarget + (x+1);

		float avgInc = randY[curTarget];
		int avgCount = 1;

		if(col < (x))
		{
			avgInc += randY[right];
			avgCount++;
		}
		if(col > 0)
		{
			avgInc += randY[left];
			avgCount++;
		}
		if(row > 0)
		{
			avgInc += randY[below];
			avgCount++;
		}
		if(row < (z)) 
		{
			avgInc += randY[above];
			avgCount++;
		}

		randYBuffer[curTarget] = avgInc / avgCount;

		col++;
		if(col >= (x+1))
		{
			col = 0;
			row++;  
		}
	}
	randY = randYBuffer;

}

void MeshData::load(const std::string& path)
{
	loadFile(path);

	load();
}

void MeshData::load(GLenum usage)
{
	createBuffers(usage);
}

bool MeshData::loadFile(const std::string& path)
{

		Assimp::Importer importer;

		const aiScene* scene = importer.ReadFile(path, aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals | aiProcess_Triangulate | aiProcess_RemoveComponent/* | aiProcess_SortByPType*/);
		if(!scene) {
			logWarn(importer.GetErrorString());
			return false;
		}
		const aiMesh* mesh = scene->mMeshes[0]; // In this simple example code we always use the 1st mesh (in OBJ files there is often only one anyway)

		// Fill vertices positions
		mVertices.reserve(mesh->mNumVertices);
		for(unsigned int i=0; i<mesh->mNumVertices; i++){
			aiVector3D pos = mesh->mVertices[i];
			mVertices.push_back(glm::vec3(pos.x, pos.y, pos.z));
		}

		// Fill vertices texture coordinates
		mUvs.reserve(mesh->mNumVertices);
		for(unsigned int i=0; i<mesh->mNumVertices; i++){
			aiVector3D UVW = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
			mUvs.push_back(glm::vec2(UVW.x, UVW.y));
		}

		// Fill vertices normals
		mNormals.reserve(mesh->mNumVertices);
		for(unsigned int i=0; i<mesh->mNumVertices; i++){
			aiVector3D n = mesh->mNormals[i];
			mNormals.push_back(glm::vec3(n.x, n.y, n.z));
		}


		// Fill face indices
		mIndices.reserve(3*mesh->mNumFaces);
		for (unsigned int i=0; i<mesh->mNumFaces; i++){
			// Assume the model has only triangles.
			mIndices.push_back(mesh->mFaces[i].mIndices[0]);
			mIndices.push_back(mesh->mFaces[i].mIndices[1]);
			mIndices.push_back(mesh->mFaces[i].mIndices[2]);
		}

		// The "scene" pointer will be deleted automatically by "importer"
		return true;
}

void MeshData::create(const std::vector<unsigned short>& indices, const std::vector<glm::vec3>& vertices, const std::vector<glm::vec2>& uvs, const std::vector<glm::vec3>& normals, GLenum usage)
{
	mIndices = indices;
	mNormals = normals;
	mUvs = uvs;
	mVertices = vertices;

	load(usage);
}

void MeshData::createBuffers(GLenum usage)
{
	// Bind VAO
	glGenVertexArrays(1, &mVertexArrayId);
	glBindVertexArray(mVertexArrayId);

	// Vertices Buffer
	mVertexBufferId = createBuffer(mVertices, 0, usage);
	// UV Buffer
	mUvBufferId = createBuffer(mUvs, 1, usage);
	// Normal Buffer
	mNormalBufferId = createBuffer(mNormals, 2, usage);
	// Index buffer
	mIndexBufferId = createBuffer(mIndices, usage);

	// Unbind VAO
	glBindVertexArray(0);
}

GLuint MeshData::createBuffer(const std::vector<unsigned short>& buff, GLenum usage)
{
	GLuint bufferId;
	
	glGenBuffers(1, &bufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buff.size() * sizeof(unsigned short), &buff[0], usage);

	return bufferId;
}

GLuint MeshData::createBuffer(const std::vector<glm::vec2>& buff, GLuint index, GLenum usage)
{
	GLuint bufferId;
	
	glGenBuffers(1, &bufferId);
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glBufferData(GL_ARRAY_BUFFER, buff.size() * sizeof(glm::vec2), &buff[0], usage);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	return bufferId;
}

GLuint MeshData::createBuffer(const std::vector<glm::vec3>& buff, GLuint index, GLenum usage)
{
	GLuint bufferId;

	glGenBuffers(1, &bufferId);
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glBufferData(GL_ARRAY_BUFFER, buff.size() * sizeof(glm::vec3), &buff[0], usage);
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	return bufferId;
}

void MeshData::updateBuffer(const std::vector<unsigned short>& buff, GLuint bufferId)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, buff.size() * sizeof(unsigned short), &buff[0], GL_DYNAMIC_DRAW);
}

void MeshData::updateBuffer(const std::vector<glm::vec2>& buff, GLuint bufferId)
{
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glBufferData(GL_ARRAY_BUFFER, buff.size() * sizeof(glm::vec2), &buff[0], GL_DYNAMIC_DRAW);
}

void MeshData::updateBuffer(const std::vector<glm::vec3>& buff, GLuint bufferId)
{
	glBindBuffer(GL_ARRAY_BUFFER, bufferId);
	glBufferData(GL_ARRAY_BUFFER, buff.size() * sizeof(glm::vec3), &buff[0], GL_DYNAMIC_DRAW);
}