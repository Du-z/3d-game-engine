#ifndef GAME_WORLD_H
#define GAME_WORLD_H

#include "app\gameWorld\gameObj.h"

class GameWorld : public ABEventReciver
{
public:
	~GameWorld();
	static GameWorld* GetInstance();

	void setup(const std::string& dir); // loads game objects from json file
	GameObj* addObj(GameObj* gameObj);

	void tick(const float deltaT);

	virtual void eventReceiver(ABEvent* theEvent);

private:
	GameWorld();
	static GameWorld* mInstance;

	std::vector<GameObj*> mGameObjList;
};

#endif // GAME_WORLD_H