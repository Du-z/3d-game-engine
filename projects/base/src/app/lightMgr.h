#ifndef LIGHT_MAN_H
#define LIGHT_MAN_H

#include <vector>
#include <string>

#include "lightMgr/light.h"

class LightMgr
{
public:
	~LightMgr();
	static LightMgr* GetInstance();

	void setup(const std::string& dir); // loads lights from json file

	void getClosestLights(Light::Type typeToGet, const int count, std::vector<LightData*>& lightList); // creates an array of the first count light data (later it will be the count closest)

private:
	LightMgr();
	static LightMgr* mInstance;

	Light::Type strToLightType(const std::string& str);

	std::vector<LightData> mLightList;
};

#endif // LIGHT_MAN_H