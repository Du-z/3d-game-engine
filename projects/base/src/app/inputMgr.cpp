#include "inputMgr.h"

#include "app/eventMgr.h"

double InputMgr::mLastXPos = 0;
double InputMgr::mLastYPos = 0;

InputMgr* InputMgr::mInstance = NULL;

InputMgr* InputMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new InputMgr();

	return mInstance;
}

InputMgr::InputMgr()
{
}

InputMgr::~InputMgr()
{
	mInstance = NULL;
}


void InputMgr::setup(GLFWwindow* window)
{
	glfwSetKeyCallback(window, keyCallback);

	glfwSetCursorPosCallback(window, cursorCallback);
	glfwGetCursorPos(window, &mLastXPos, &mLastYPos);
}

void InputMgr::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	switch(action)
	{
	case GLFW_PRESS:
		EventMgr::GetInstance()->addEvent(new KeyPressed(key, mods));
		break;
	case GLFW_RELEASE:
		EventMgr::GetInstance()->addEvent(new KeyReleased(key, mods));
		break;
	case GLFW_REPEAT:
		EventMgr::GetInstance()->addEvent(new KeyRepeat(key, mods));
		break;
	}
}

void InputMgr::cursorCallback(GLFWwindow* window, double xPos, double yPos)
{	
	double deltaXPos = xPos - mLastXPos;
	double deltaYPos = yPos - mLastYPos;
	mLastXPos = xPos;
	mLastYPos = yPos;
	
	EventMgr::GetInstance()->addEvent(new CursorMoved(xPos, yPos, deltaXPos, deltaYPos));
}