#include "resMgr.h"

#include <fstream>
#include <SOIL.h>

#include "app\logger.h"


ResMgr* ResMgr::mInstance = NULL;
ResMgr* ResMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new ResMgr();

	return mInstance;
}

ResMgr::ResMgr()
{
	
}

ResMgr::~ResMgr()
{
	mInstance = NULL;
}

MeshData* ResMgr::addMesh(const std::string& name, MeshData* meshData)
{
	if (meshList.find(name) == meshList.end())
	{
		logInfo("Added mesh: '" << name << "'");
		meshList[name] = meshData;
		return meshData;
	}
	else
	{
		// It's already loaded!
		logWarn("'" << name << "' has already been added to the list of meshs. Use another name for this mesh.");
		return meshList[name];
	}
}

MeshData* ResMgr::genMesh(const std::string& name, const int x, const int y, const float quadSize, const float maxU, const float maxV, const float randY, const int smoothPasses)
{
	if (meshList.find(name) == meshList.end())
	{
		// It doesn't exists! load it!

		MeshData* mesh = new MeshData();

		mesh->generateVerts(x, y, quadSize, randY, smoothPasses);
		mesh->generateInd(x, y);
		mesh->generateNorm();
		mesh->generateUv(x, y, maxU, maxV);

		// add the mesh to the GPU
		mesh->load(GL_DYNAMIC_DRAW);

		if(mesh->getVertexArrayId() != 0)
		{
			logInfo("Generated mesh: '" << name << "'");
			meshList[name] = mesh;
			return mesh;
		}
		logWarn("Could not generate mesh: '" << name << "'");
		delete mesh;
	}
	else
	{
		// It's already loaded!
		return meshList[name];
	}

	return NULL;
}

MeshData* ResMgr::getMesh(const std::string& dir)
{
	if (meshList.find(dir) == meshList.end())
	{
		// It doesn't exists! load it!

		MeshData* mesh = new MeshData();

		mesh->load("rsc/models/" + dir);

		if(mesh->getVertexArrayId() != 0)
		{
			logInfo("Loaded mesh: '" << dir << "'");
			meshList[dir] = mesh;
			return mesh;
		}
		logWarn("Could not load mesh: '" << dir << "'");
		delete mesh;
	}
	else
	{
		// It's already loaded!
		return meshList[dir];
	}

	return NULL;
}

void ResMgr::disposeMesh(const std::string& dir)
{
	auto it = meshList.find(dir);

	if (it != meshList.end())
	{
		delete it->second;
		meshList.erase(it);
	}
}

void ResMgr::disposeAllMeshs()
{
	for (auto it = meshList.begin(); it != meshList.end(); ++it)
	{
		delete it->second;
	}

	meshList.clear();
}

GLuint ResMgr::getTexture(const std::string& dir, bool useMipMaps)
{
	std::stringstream ss;
	ss << dir << useMipMaps;
	std::string name = ss.str();
	
	if (textureList.find(name) == textureList.end())
	{
		// It doesn't exists! load it!

		int MipMapsFlag = 0;
		if(useMipMaps)
			MipMapsFlag = SOIL_FLAG_MIPMAPS;
		GLuint* result = new GLuint();
			
		*result = SOIL_load_OGL_texture
			(("rsc/textures/" + dir).c_str(),
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			MipMapsFlag | SOIL_FLAG_INVERT_Y | SOIL_FLAG_POWER_OF_TWO// | SOIL_FLAG_COMPRESS_TO_DXT
			);

		if(*result != 0)
		{
			logInfo("Loaded texture: '" << dir << "' MipMaps  = " << Utilities::boolToString(useMipMaps));
			textureList[name] = result;
			return *result;
		}
		logWarn("Could not load texture: '" << dir << "' " << SOIL_last_result());
		delete result;
	}
	else
	{
		// It's already loaded!
		return *textureList[name];
	}

	return NULL;
}

void ResMgr::disposeTexture(const std::string& dir, bool useMipMaps)
{
	std::stringstream ss;
	ss << dir << useMipMaps;
	std::string name = ss.str();
	
	auto it = textureList.find(name);

	if (it != textureList.end())
	{
		GLuint toDelete = *it->second;
		glDeleteTextures(1, &toDelete);

		delete it->second;
		textureList.erase(it);
	}
}

void ResMgr::disposeAllTextures()
{
	for (auto it = textureList.begin(); it != textureList.end(); ++it)
	{
		GLuint toDelete = *it->second;
		glDeleteTextures(1, &toDelete);

		delete it->second;
	}
	
	textureList.clear();
}

ShaderData* ResMgr::getShader(const std::string& vertexDir, const std::string& fragDir)
{
	std::string name = vertexDir + fragDir;
	
	if (shaderList.find(name) == shaderList.end())
	{
		// It doesn't exists! load it!

		GLuint result = LoadShaders(("rsc/shaders/" + vertexDir).c_str(), ("rsc/shaders/" + fragDir).c_str());

		if(result != 0)
		{
			logInfo("Loaded shaders: '" << vertexDir << "' and '" << fragDir << "'");
			
			ShaderData* shaderData = new ShaderData();
			
			shaderData->ProgramId = result;

			glUseProgram(result);

			shaderData->MvpId = glGetUniformLocation(result, "MVP");
			shaderData->ModelMatrixId = glGetUniformLocation(result, "M");
			shaderData->ViewMatrixId = glGetUniformLocation(result, "V");
			shaderData->ProjectionMatrixId = glGetUniformLocation(result, "P");
			shaderData->MvInvTransMatrixId = glGetUniformLocation(result, "MV_inv_trans");

			shaderData->DiffuseTextureId = glGetUniformLocation(result, "diffuseSampler");
			shaderData->NormalTextureId = glGetUniformLocation(result, "normalSampler");
			shaderData->SpecularTextureId = glGetUniformLocation(result, "specularSampler");
			shaderData->EmissiveTextureId = glGetUniformLocation(result, "emissiveSampler");

			shaderData->CameraPosId = glGetUniformLocation(result, "Camera_worldspace");

			shaderData->LightPosId = glGetUniformLocation(result, "LightPosition_worldspace");
			shaderData->AmbientTintId = glGetUniformLocation(result, "Ambient");
			shaderData->SpecularTintId = glGetUniformLocation(result, "Specular");
			shaderData->DiffuseTintId = glGetUniformLocation(result, "Diffuse");
			shaderData->SpecularPowerId = glGetUniformLocation(result, "SpecularPower");
			
			shaderList[name] = shaderData;
			return shaderData;
		}
			logWarn("Could not load shaders: '" << vertexDir << "' and '" << fragDir << "'");
	}
	else
	{
		// It's already loaded!
		return shaderList[name];
	}

	return NULL;
}

void ResMgr::disposeShader(const std::string& vertexDir, const std::string& fragDir)
{
	std::string name = vertexDir + fragDir;
	
	auto it = shaderList.find(name);

	if (it != shaderList.end())
	{
		glDeleteShader(it->second->ProgramId);
		delete it->second;
		shaderList.erase(it);
	}
}

void ResMgr::disposeAllShaders()
{
	for (auto it = shaderList.begin(); it != shaderList.end(); ++it)
	{
		glDeleteShader(it->second->ProgramId);
		delete it->second;
	}

	shaderList.clear();
}

Json::Value* ResMgr::getJson(const std::string& dir)
{

	if (jsonList.find(dir) == jsonList.end())
	{
		// It doesn't exists! load it!

		Json::Value* val = new Json::Value;
		Json::Reader reader;
		bool result = reader.parse(std::ifstream("rsc/json/" + dir, std::ifstream::binary), *val, false);

		if(result)
		{
			logInfo("Loaded json: '" << dir << "'");
			jsonList[dir] = val;
			return val;
		}
		logWarn("Could not load json: '" << dir << "' " << reader.getFormatedErrorMessages());
		delete val;
	}
	else
	{
		// It's already loaded!
		return jsonList[dir];
	}

	return NULL;
}

void ResMgr::disposeJson(const std::string& dir)
{
	auto it = jsonList.find(dir);

	if (it != jsonList.end())
	{
		delete it->second;
		jsonList.erase(it);
	}
}

void ResMgr::disposeAllJsons()
{
	for (auto it = jsonList.begin(); it != jsonList.end(); ++it)
	{
		delete it->second;
	}

	jsonList.clear();
}

LuaScriptData* ResMgr::getLuaScript(const std::string& dir)
{

	if (luaScriptList.find(dir) == luaScriptList.end())
	{
		// It doesn't exists! load it!

		LuaScriptData* val = new LuaScriptData();
		bool result = val->load("rsc/luaScripts/" + dir);

		if(result)
		{
			logInfo("Loaded LUA Script: '" << dir << "'");
			luaScriptList[dir] = val;
			return val;
		}
		logWarn("Could not load json: '" << dir << "'");
		delete val;
	}
	else
	{
		// It's already loaded!
		return luaScriptList[dir];
	}

	return NULL;
}

void ResMgr::disposeLuaScript(const std::string& dir)
{
	auto it = luaScriptList.find(dir);

	if (it != luaScriptList.end())
	{
		delete it->second;
		luaScriptList.erase(it);
	}
}

void ResMgr::disposeAllLuaScripts()
{
	for (auto it = luaScriptList.begin(); it != luaScriptList.end(); ++it)
	{
		delete it->second;
	}

	jsonList.clear();
}

void ResMgr::disposeAll()
{
	disposeAllMeshs();
	disposeAllTextures();
	disposeAllShaders();
	disposeAllJsons();
	disposeAllLuaScripts();
}