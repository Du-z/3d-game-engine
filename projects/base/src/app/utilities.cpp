#include "utilities.h"

#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

Utilities::Utilities()
{
}

Utilities::~Utilities()
{
}

int Utilities::randInt(int min, int max)
{
	int retVal = min + ((rand() * rand()) % (int)(max - min + 1));
	return retVal;
}

unsigned int Utilities::randUInt(const unsigned int min, const unsigned int max)
{
	unsigned int retVal = min + ((rand() * rand()) % (unsigned int)(max - min + 1));
	return retVal;
}

float Utilities::randFloat(float min, float max)
{
	float r = (float)rand() / (float)RAND_MAX;
	float retVal = min + r * (max - min);
	return retVal;
}

float Utilities::radToDeg(const float rad)
{
	return rad * 180 / PI;
}

float Utilities::degToRad(const float deg)
{
	return deg / 180 * PI;
}

std::string Utilities::getFileNameFromDir(const std::string& fileDir)
{
	std::string filename;

	size_t pos = fileDir.find_last_of("\\");
	if(pos != std::string::npos)
		filename.assign(fileDir.begin() + pos + 1, fileDir.end());
	else
		filename = fileDir;

	return filename;
}

// Get current date/time, format is YYYY-MM-DD HH:mm:ss
std::string Utilities::currentDateTime()
{
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[40];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

	return buf;
}

// Get current date/time, format is YY-MM-DD_HH-MM-SS
std::string Utilities::currentDateTimeDirSafe()
{
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[40];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%y-%m-%d_%H-%M-%S", &tstruct);

	return buf;
}

unsigned int Utilities::hash(const std::string& str)
{
	unsigned int theHash = 5381;

	for(int i = 0; i < str.length(); i++)
	{
		theHash = ((theHash << 5) + theHash) + str[i];
	}

	return theHash;
}