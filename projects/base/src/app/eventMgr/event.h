#ifndef EVENT_H
#define EVENT_H

#include "app\utilities.h"

namespace Event
{
	enum Types
	{
		UNSET = -1,

		KEY_PRESSED,
		KEY_RELEASED,
		KEY_REPEAT,

		CURSOR_MOVED,

		COUNT
	};
}

class ABEvent
{
public:
	ABEvent(){mEventType = Event::UNSET;}
	virtual ~ABEvent(){}

protected:
	PROP_G(Event::Types, mEventType, EventType);

};

class ABEventReciver
{
public:
	ABEventReciver(){}
	virtual ~ABEventReciver(){}
	virtual void eventReceiver(ABEvent* theEvent){}
};

/////// KEYBOARD EVENTS \\\\\\\

class KeyPressed : public ABEvent
{
public:
	KeyPressed(const int key, const int mods);
	~KeyPressed(){}
private:
	KeyPressed(){}
	PROP_G(int, mKey, Key);
	PROP_G(int, mMods, Mods);
};

class KeyReleased : public ABEvent
{
public:
	KeyReleased(const int key, const int mods);
	~KeyReleased(){}
private:
	KeyReleased(){}
	PROP_G(int, mKey, Key);
	PROP_G(int, mMods, Mods);
};

class KeyRepeat : public ABEvent
{
public:
	KeyRepeat(const int key, const int mods);
	~KeyRepeat(){}
private:
	KeyRepeat(){}
	PROP_G(int, mKey, Key);
	PROP_G(int, mMods, Mods);
};

class CursorMoved : public ABEvent
{
public:
	CursorMoved(const double xPos, const double yPos, const double deltaXPos, const double deltaYPos);
	~CursorMoved(){}
private:
	CursorMoved(){}
	PROP_G(double, mXPos, XPos);
	PROP_G(double, mYPos, YPos);

	PROP_G(double, mDeltaXPos, DeltaXPos);
	PROP_G(double, mDeltaYPos, DeltaYPos);
};

#endif // EVENT_H