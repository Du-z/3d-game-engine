#include "event.h"

KeyPressed::KeyPressed(const int key, const int mods)
{
	mEventType = Event::KEY_PRESSED;

	mKey = key;
	mMods = mods;
}

KeyReleased::KeyReleased(const int key, const int mods)
{
	mEventType = Event::KEY_RELEASED;
	
	mKey = key;
	mMods = mods;
}

KeyRepeat::KeyRepeat(const int key, const int mods)
{
	mEventType = Event::KEY_REPEAT;

	mKey = key;
	mMods = mods;
}

CursorMoved::CursorMoved(const double xPos, const double yPos, const double deltaXPos, const double deltaYPos)
{
	mEventType = Event::CURSOR_MOVED;

	mXPos = xPos;
	mYPos = yPos;

	mDeltaXPos = deltaXPos;
	mDeltaYPos = deltaYPos;
}