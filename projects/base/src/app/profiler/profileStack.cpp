#include "profileStack.h"

#include "GLFW/glfw3.h"

#include "app/profiler.h"

ProfileStack::ProfileStack()
{	
	mNextLvl = "";

	const int size = Profiler::GetInstance()->getSecondIndexCount();
	mProfileData.resize(size);

	for(int i = 0; i < size; i++)
	{
		mProfileData[i].timesRun = 0;
		mProfileData[i].time = 0;
	}
}

ProfileStack::~ProfileStack()
{

}

void ProfileStack::upStackStart(const std::string& name)
{
	if(mNextLvl.size() == 0) // this is the highest level of the current stack
	{
		mProfileStack[name].start(name); // so we should start the next

		mNextLvl = name;
	}
	else
	{
		mProfileStack[mNextLvl].upStackStart(name);
	}
}

void ProfileStack::start(const std::string& name)
{
	if(mName.size() == 0)
		mName = name;

	timeStarted = glfwGetTime();
}

bool ProfileStack::stop(const std::string& name)
{
	if(mNextLvl.size() == 0) // this is the highest level of the current stack
	{
		// Do timing stuff here
		int i = Profiler::GetInstance()->getSecondIndex();
		mProfileData[i].timesRun++;
		mProfileData[i].time += glfwGetTime() - timeStarted;

		return true;
	}
	else
	{
		bool result = mProfileStack[mNextLvl].stop(name);

		if(result) // we just stopped the highest level
		{
			mNextLvl = "";
		}

		return false;
	}
}

void ProfileStack::startSecond()
{
	for (auto it = mProfileStack.begin(); it != mProfileStack.end(); ++it)
	{
		it->second.startSecond();
	}

	int i = Profiler::GetInstance()->getSecondIndex();

	mProfileData[i].timesRun = 0;
	mProfileData[i].time = 0;
}

void ProfileStack::toFile(std::ofstream& file, int level)
{

	for(int i = 0; i < level; i++)
	{
		file << '\t';
	}

	updAvgProfileData();
	
	file << mName << "() " <<
		" Run: " << mAvgProfileData.timesRun << " times" <<
		" Time: " << mAvgProfileData.time << "ms" <<
		std::endl;

	level++;
	for (auto it = mProfileStack.begin(); it != mProfileStack.end(); ++it)
	{
		it->second.toFile(file, level);
	}
}

void ProfileStack::updAvgProfileData()
{
	mAvgProfileData.timesRun = 0;
	mAvgProfileData.time = 0;

	for(int i = 0; i < mProfileData.size(); i++)
	{
		mAvgProfileData.timesRun += mProfileData[i].timesRun;
		mAvgProfileData.time += mProfileData[i].time;
	}

	mAvgProfileData.timesRun /= mProfileData.size();
	mAvgProfileData.time /= mProfileData.size();
}

void ProfileStack::getTime(int depth, int timeIndex, std::vector<TimeData>& timeData)
{
	if(depth == 0)
	{
		TimeData data;
		data.time = mProfileData[timeIndex].time;
		data.name = &mName;
		
		timeData.push_back(data);
	}
	else
	{
		depth--;
		for (auto it = mProfileStack.begin(); it != mProfileStack.end(); ++it)
		{
			it->second.getTime(depth, timeIndex, timeData);
		}
	}
}