#include "profile.h"

Profile::Profile(const std::string& name)
{	
	mName = name;
	start();
}

Profile::~Profile()
{
	stop();
}

void Profile::start()
{
	Profiler::GetInstance()->start(mName);
}

void Profile::stop()
{
	Profiler::GetInstance()->stop(mName);
}