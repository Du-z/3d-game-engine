#ifndef PROFILE_H
#define PROFILE_H

#include <string>
#include <BaseTsd.h>

#include <ostream>

#include "app\utilities.h"
#include "app\profiler.h"

#if defined(DEBUG) || defined(_DEBUG)
#define profile() Profile __profile__(__FUNCTION__)
#define profileEndFrame() Profiler::GetInstance()->endFrame()
#else
#define profile() Profile __profile__(__FUNCTION__)
#define profileEndFrame() Profiler::GetInstance()->endFrame()
#endif

class Profile
{
public:
	Profile(const std::string& name);
	~Profile();

private:
	Profile();

	void start(); // returns false if start() has already been called without calling stop() first
	void stop(); // returns false if start() has not been called yet

	PROP_G(std::string, mName, Name);
};

#endif // PROFILE_H