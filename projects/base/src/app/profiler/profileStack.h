#ifndef PROFILE_STACK_H
#define PROFILE_STACK_H

#include <map>
#include <vector>

#include "app/utilities.h"
#include "app/logger.h"

#include <iostream>
#include <fstream>

struct ProfileData
{
	int timesRun;
	float time;
};

struct TimeData
{
	std::string* name;
	float time;
};

class ProfileStack
{
public:
	ProfileStack();
	~ProfileStack();

	void upStackStart(const std::string& name);

	void start(const std::string& name);
	// returns true when this was the top most on the stack
	bool stop(const std::string& name);

	void startSecond();

	void toFile(std::ofstream& file, int level);

	void getTime(int depth, int timeIndex, std::vector<TimeData>& timeData);

private:
	std::string mName;

	std::map<std::string, ProfileStack> mProfileStack;
	PROP_G(std::string, mNextLvl, mNextLvl);

	std::vector<ProfileData> mProfileData;
	ProfileData mAvgProfileData;

	void updAvgProfileData();

	float timeStarted;
};

#endif // PROFILE_STACK_H