#ifndef RES_MAN_H
#define RES_MAN_H

#include <map>
#include <vector>
#include <string>

#include <GL\glew.h>
#include <glm\glm.hpp>

#include <json\json.h>

#include "app\resMgr\meshData.h"
#include "app\resMgr\luaScriptData.h"
#include "app\resMgr\shaderData.h"

struct ShaderData
{
	GLuint ProgramId;

	GLuint MvpId;
	GLuint ProjectionMatrixId;
	GLuint ViewMatrixId;
	GLuint ModelMatrixId;
	GLuint MvInvTransMatrixId;

	GLuint DiffuseTextureId;
	GLuint NormalTextureId;
	GLuint SpecularTextureId;
	GLuint EmissiveTextureId;

	GLuint CameraPosId;

	GLuint LightPosId;
	GLuint AmbientTintId;
	GLuint SpecularTintId;
	GLuint DiffuseTintId;
	GLuint SpecularPowerId;
};

class ResMgr
{
public:
	~ResMgr();
	static ResMgr* GetInstance();

	MeshData* addMesh(const std::string& name, MeshData* meshData);
	MeshData* genMesh(const std::string& name, const int x, const int y, const float quadSize = 1, const float maxU = 1, const float maxV = 1, const float randY = 0, const int smoothPasses = 0);
	MeshData* getMesh(const std::string& dir);
	void disposeMesh(const std::string& dir);
	void disposeAllMeshs();

	GLuint getTexture(const std::string& dir, bool useMipMaps = true);
	void disposeTexture(const std::string& dir, bool useMipMaps = true);
	void disposeAllTextures();

	ShaderData* getShader(const std::string& vertexDir, const std::string& fragDir);
	void disposeShader(const std::string& vertexDir, const std::string& fragDir);
	void disposeAllShaders();

	Json::Value* getJson(const std::string& dir);
	void disposeJson(const std::string& dir);
	void disposeAllJsons();

	LuaScriptData* getLuaScript(const std::string& dir);
	void disposeLuaScript(const std::string& dir);
	void disposeAllLuaScripts();

	void disposeAll();

private:
	ResMgr();
	static ResMgr* mInstance;

	std::map<std::string, MeshData*> meshList;
	std::map<std::string, GLuint*> textureList;
	std::map<std::string, ShaderData*> shaderList;
	std::map<std::string, Json::Value*> jsonList;
	std::map<std::string, LuaScriptData*> luaScriptList;
};

#endif // RES_MAN_H