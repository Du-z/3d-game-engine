#include "logger.h"

#include <stddef.h>

#include <iostream>
#include <fstream>

Logger* Logger::mInstance = NULL;

Logger* Logger::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new Logger();

	return mInstance;
}

Logger::Logger()
{
	setDir("log.txt");
	clearLog();
}

Logger::~Logger()
{
	mInstance = NULL;
}

void Logger::setDir(const std::string& dir)
{
	mDir = dir;
}

void Logger::writeLog(const std::string& str)
{
	std::ofstream file(mDir, std::ios::app);
	if (file.is_open())
	{		
		file << str;
	
		file.close();
	}

	std::cout << str;
}

void Logger::clearLog()
{

	std::ofstream file(mDir, std::ios::trunc);
	if (file.is_open())
	{
		file << "Max Log Level = " << CUR_LOG_LEVEL << '\n';
		file.close();

		std::cout << "Max Log Level = " << CUR_LOG_LEVEL << '\n';
	}
}