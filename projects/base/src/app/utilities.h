#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>
#include <limits>
#include <float.h>

#define PI 3.14159265358979323846

#define TOSTRING2(a)	#a
#define TOSTRING(a)		TOSTRING2(a)

#define PROP_G_S(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	void set ## accessBy(dataType a) {dataName = a;}		\
	dataType get ## accessBy() {return dataName;}			\
	protected:

#define PROP_GR_S(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	void set ## accessBy(dataType a) {dataName = a;}		\
	dataType & get ## accessBy() {return dataName;}			\
	protected:

#define PROP_GP_S(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	void set ## accessBy(dataType a) {dataName = a;}		\
	dataType * get ## accessBy() {return &dataName;}		\
	protected:

#define PROP_G(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	dataType get ## accessBy() const {return dataName;}		\
	protected:

#define PROP_GR(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	dataType & get ## accessBy() {return dataName;}			\
	protected:

#define PROP_GP(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	dataType * get ## accessBy() {return &dataName;}		\
	protected:

#define PROP_S(dataType, dataName, accessBy)				\
	protected:												\
	dataType dataName;										\
	public:													\
	void set ## accessBy(dataType a) {dataName = a;}		\
	protected:

class Utilities
{
public:

	static int randInt(){return randInt(INT_MIN, INT_MAX);}
	static int randInt(const int min, const int max);
	static unsigned int randUInt(){return randUInt(0, UINT_MAX);}
	static unsigned int randUInt(const unsigned int min, const unsigned int max);
	static float randFloat(){return randFloat(FLT_MIN, FLT_MAX);}
	static float randFloat(const float min, const float max);

	static float radToDeg(const float rad);
	static float degToRad(const float deg);

	static std::string getFileNameFromDir(const std::string& fileDir);

	static std::string boolToString(const bool b)
	{
		return b ? "true" : "false";
	}

	static std::string currentDateTime();
	static std::string currentDateTimeDirSafe();

	static unsigned int hash(const std::string& str);

private:
	Utilities();
	~Utilities();
};

#endif // UTILITIES_H