#ifndef APP_H
#define APP_H

// Include GLEW first
#include <GL\glew.h>
#include <GLFW\glfw3.h>

#include "btBulletDynamicsCommon.h"

#include "app/utilities.h"

#include "errorCodes.h"

class App
{
public:
	~App();
	static App* GetInstance();

	Error::Code setup();
	void loop();
	void dispose();

private:
	App();
	static App* mInstance;

	PROP_G(GLFWwindow*, mWindow, Window);
	Error::Code rendererSetup();

	bool mIsRunning;

	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btSequentialImpulseConstraintSolver* solver;

	PROP_G(btDiscreteDynamicsWorld*, mDynamicsWorld, DynamicsWorld);
	void dynamicWorldStep(const float deltaT);
	void dynamicWorldSetup();
};

#endif // APP_H