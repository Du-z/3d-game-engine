#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal_modelspace;

// Output data ; will be interpolated for each fragment.
out vec2 UV;

out vec3 Normal;
out vec3 ViewDirection_cameraspace;
out vec3 LightDirection_cameraspace;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 M;
uniform mat4 V;
uniform mat3 MV_inv_trans;

uniform vec3 LightPosition_worldspace;

uniform vec3 Camera_worldspace;

void main(void)
{
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
   
	UV = vertexUV;
   
	vec4 Position_cameraspace = V * M * vec4(vertexPosition_modelspace, 1);
	
	vec4 LightPosition_cameraspace = V * vec4(LightPosition_worldspace, 1);
	
	ViewDirection_cameraspace = vec3(0,0,0) - Position_cameraspace.xyz;
	LightDirection_cameraspace = LightPosition_cameraspace.xyz - Position_cameraspace.xyz;
	
	Normal = MV_inv_trans * vertexNormal_modelspace; 
}