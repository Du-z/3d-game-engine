#version 330 core

// Ouput data
out vec4 color;

// Interpolated values from the vertex shaders
in vec2 UV;

// Values that stay constant for the whole mesh.
uniform vec4 Ambient = vec4(0.1, 0.1, 0.1, 1);

uniform sampler2D diffuseSampler;
uniform sampler2D emissiveSampler;

void main(void)
{   
   vec4 BaseColor = texture2D(diffuseSampler, UV);
   vec4 EmissiveColor = texture2D(emissiveSampler, UV);
   
   vec4 TotalAmbient = (Ambient * BaseColor) + EmissiveColor;
  
   color = TotalAmbient;
}