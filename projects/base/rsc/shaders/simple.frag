#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D diffuseSampler;

void main()
{
	// texture colour shading
	color = texture2D( diffuseSampler, UV );
}