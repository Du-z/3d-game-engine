#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_screenspace;
layout(location = 1) in vec2 vertexUV;

// Output data ; will be interpolated for each fragment.
out vec2 UV;

// Values that stay constant for the whole mesh.
uniform mat4 M;

void main(){
	vec2 res = vec2(1280 / 2, 720 / 2);
	// Output position of the vertex, in clip space
	// map [0->res.x][0->res.y] to [-1->1][-1->1]	
	vec2 vertexPosition_homoneneousspace = vertexPosition_screenspace.xy - res;
	vertexPosition_homoneneousspace /= res;
	gl_Position =  M * vec4(vertexPosition_homoneneousspace,0,1);
	
	// UV of the vertex. No special space for this one.
	UV = vertexUV;
}