#version 330 core 

// Ouput data
out vec4 color;

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 ViewDirection_cameraspace;
in vec3 LightDirection_cameraspace;
in vec3 Normal;

// Values that stay constant for the whole mesh.
uniform vec4 Specular = vec4(1.f, 1.f, 1.f, 1.f);
uniform vec4 Diffuse = vec4(1.f, 1.f, 1.f, 1.f);
uniform float SpecularPower = 10; 

uniform sampler2D diffuseSampler;
uniform sampler2D normalSampler;
uniform sampler2D specularSampler;

mat3 cotangent_frame(vec3 N, vec3 p, vec2 UVcoord)
{
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx(p);
    vec3 dp2 = dFdy(p);
    vec2 duv1 = dFdx(UVcoord);
    vec2 duv2 = dFdy(UVcoord);
 
    // solve the linear system
    vec3 dp2perp = cross(dp2, N);
    vec3 dp1perp = cross(N, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;
 
    // construct a scale-invariant frame 
    float invmax = inversesqrt(max(dot(T,T), dot(B,B)));
    return mat3(T * invmax, B * invmax, N);
}

vec3 perturb_normal(vec3 N, vec3 V, vec2 UVcoord, sampler2D nmap)
{
    // assume N, the interpolated vertex normal and 
    // V, the view vector (vertex to eye)
    vec3 map = texture2D(nmap, UVcoord).xyz;
//#ifdef WITH_NORMALMAP_UNSIGNED
    map = map * 255./127. - 128./127.;
/*
#endif
#ifdef WITH_NORMALMAP_2CHANNEL
    map.z = sqrt( 1. - dot( map.xy, map.xy ) );
#endif
#ifdef WITH_NORMALMAP_GREEN_UP
    map.y = -map.y;
#endif
*/
    mat3 TBN = cotangent_frame(N, -V, UVcoord);;
    return normalize(TBN * map);
}

void main(void)
{
   
   float L = length(LightDirection_cameraspace);
   vec3 NLightDirection_cameraspace = normalize(LightDirection_cameraspace);
   vec3 NViewDirection_cameraspace = normalize(ViewDirection_cameraspace);
   vec3	NNormal = normalize(Normal);
   
   NNormal = perturb_normal(NNormal, NViewDirection_cameraspace, UV, normalSampler);
   
   float NDotL = dot(NNormal, NLightDirection_cameraspace); 
   
   vec3 Reflection = normalize(((2.0 * NNormal ) * NDotL) - NLightDirection_cameraspace); 
   float RDotV = max(0.0, dot(Reflection, NViewDirection_cameraspace));
   
   vec4 BaseColor = texture2D(diffuseSampler, UV);
   vec4 SpecularColor = texture2D(specularSampler, UV);
   
   vec4 TotalDiffuse = Diffuse * NDotL * BaseColor / (L*0.1+1); 
   vec4 TotalSpecular = Specular * (pow(RDotV, SpecularPower)) * SpecularColor / (L*0.1+1);
  
   //color = TotalDiffuse + TotalSpecular;     
	color = (TotalDiffuse + TotalSpecular);      
	
}